"""
SIMULATED ENVIRONMENT representing the PRE-OPERATIVE SIMULATION.
Tissue grasping is triggered by ROS; tissue points within a certain distance from the EE are grasped (if any).
To launch this scene: runSofa simulation.py 
Default config file loaded: ./config/settings.yml
"""
import sys, os
import numpy as np
import math, random
import time, copy
from scipy.spatial.transform import Rotation as R

# SIMULATION
import Sofa
from vtk import *
sys.path.append('utils')
import utils
from extract_random_patch import extractRandomFixed
sys.path.append('utils/components')
from tissue import Tissue, Material
from solver import Solver
from boundaries import FixedBoundaries
from ee import EE
from target import Target
from camera import Camera
from plane import Plane
from points import Points

config_file = './config/settings.yml'
options = utils.load_yaml(config_file)
print "Loaded config from ", config_file

abs_path = os.path.dirname(os.path.abspath(__file__))

##################################
####### SETUP ROS
##################################
import rospy
from std_msgs.msg import Int32, Float32, Float32MultiArray, Header, Bool, Int32MultiArray
from sensor_msgs import point_cloud2
from sensor_msgs.msg import PointCloud2, PointField, JointState
from geometry_msgs.msg import PoseStamped
import tf, struct

fields = [
	PointField('x', 0, PointField.FLOAT32, 1),
	PointField('y', 4, PointField.FLOAT32, 1),
	PointField('z', 8, PointField.FLOAT32, 1),
	PointField('rgba', 12, PointField.UINT32, 1),
	]
rgba_tissue = struct.unpack('I', struct.pack('BBBB', 255, 0, 255, 255))[0]
rgba_fixed  = struct.unpack('I', struct.pack('BBBB', 255, 0, 0, 255))[0]

# Init SOFA node
rospy.init_node(options['ros']['sofa_node'])

tf_world2sofa = rospy.get_param("world2sofa") 
tf_world2sofa = np.asarray(tf_world2sofa).reshape((4,4))
tf_sofa2world = np.linalg.inv(tf_world2sofa)

psm1_pose = None
psm2_pose = None
t_ecm = None
q_ecm = None
tf_list = tf.TransformListener()

psm1_grasp = False
psm2_grasp = False
grasp_pose = None
send_grasp_pose = False

do_update = False
banet_indices = []

# Callback for PSM1 and PSM2 poses
def psm1_callback(msg):
	global psm1_pose
	global tf_list
	psm1_base = msg
	psm1_base.header.frame_id = "PSM1_base"
	psm1_base.header.stamp = rospy.Time(0.)
	tf_list.waitForTransform(rospy.get_param("fixed_frame"), "PSM1_base", time=rospy.Time(0.), timeout = rospy.Duration(5.))
	psm1_world = tf_list.transformPose(rospy.get_param("fixed_frame"), psm1_base)

	rcv_pose  = [psm1_world.pose.position.x, psm1_world.pose.position.y, psm1_world.pose.position.z, 1]
	psm1_pose = list(tf_world2sofa.dot(np.asarray(rcv_pose)) )[:-1]
	
def psm2_callback(msg):
	global psm2_pose
	global tf_list
	psm2_base = msg
	psm2_base.header.frame_id = "PSM2_base"
	psm2_base.header.stamp = rospy.Time(0.)
	tf_list.waitForTransform(rospy.get_param("fixed_frame"), "PSM2_base", time=rospy.Time(0.), timeout = rospy.Duration(5.))
	psm2_world = tf_list.transformPose(rospy.get_param("fixed_frame"), psm2_base)

	rcv_pose  = [psm2_world.pose.position.x, psm2_world.pose.position.y, psm2_world.pose.position.z, 1]
	psm2_pose = list(tf_world2sofa.dot(np.asarray(rcv_pose)) )[:-1]

def ecm_callback(msg):
	global t_ecm, q_ecm
	global tf_list
	ecm_base = msg
	ecm_base.header.frame_id = "ECM_base"
	ecm_base.header.stamp = rospy.Time(0.)
	tf_list.waitForTransform(rospy.get_param("fixed_frame"), "ECM_base", time=rospy.Time(0.), timeout = rospy.Duration(5.))
	ecm_world = tf_list.transformPose(rospy.get_param("fixed_frame"), ecm_base)

	t_ecm  = [ecm_world.pose.position.x, ecm_world.pose.position.y, ecm_world.pose.position.z]
	q_ecm  = [ecm_world.pose.orientation.x, ecm_world.pose.orientation.y, ecm_world.pose.orientation.z, ecm_world.pose.orientation.w]
		

# Callback triggering grasping
def psm1_grasp_callback(msg):
	global psm1_grasp, grasp_pose, send_grasp_pose
	grasp_state = msg.position[0]
	if grasp_state < 0:
		psm1_grasp = True
		if grasp_pose is None:
			send_grasp_pose = True
	else:
		psm1_grasp = False

def psm2_grasp_callback(msg):
	global psm2_grasp, grasp_pose, send_grasp_pose
	grasp_state = msg.position[0]
	if grasp_state < 0:
		psm2_grasp = True
		if grasp_pose is None:
			send_grasp_pose = True
	else:
		psm2_grasp = False

# Callback triggering attachment points update
def update_callback(msg):
	global do_update
	do_update = msg.data
	
def banet_callback(msg):
	global banet_indices
	banet_indices = list(msg.data)

# Subscriber for PSM1 and PSM2
sub_psm1 = rospy.Subscriber(rospy.get_param("psm1_topic"), PoseStamped, psm1_callback, queue_size=1)
sub_psm2 = rospy.Subscriber(rospy.get_param("psm2_topic"), PoseStamped, psm2_callback, queue_size=1)
sub_psm1_grasp = rospy.Subscriber(rospy.get_param("psm1_jaw_topic"), JointState, psm1_grasp_callback, queue_size=1)
sub_psm2_grasp = rospy.Subscriber(rospy.get_param("psm2_jaw_topic"), JointState, psm2_grasp_callback, queue_size=1)
sub_ecm = rospy.Subscriber(rospy.get_param("ecm_topic"), PoseStamped, ecm_callback, queue_size=1)

# Subscribers for BANet and parameters update
sub_uncertain = rospy.Subscriber(rospy.get_param("uncertainty_topic"), Int32, update_callback, queue_size=1)
sub_banet_ind = rospy.Subscriber(rospy.get_param("banet_indices_topic"), Int32MultiArray, banet_callback, queue_size=1)

# Publishers from SOFA
pub_tissue  = rospy.Publisher(rospy.get_param("sofa_tissue_topic"), PointCloud2, queue_size=1)
pub_forces  = rospy.Publisher(rospy.get_param("force_topic"), Float32MultiArray, queue_size=1)
pub_stress  = rospy.Publisher(rospy.get_param("stress_topic"), Float32, queue_size=1)
pub_fixed   = rospy.Publisher(rospy.get_param("pcl_fixed_topic"), PointCloud2, queue_size=1)
pub_target  = rospy.Publisher(rospy.get_param("roi_position_topic"), Float32MultiArray, queue_size=1)
pub_grasped_tissue  = rospy.Publisher(rospy.get_param("sofa_grasped_topic"), Int32MultiArray, queue_size=1)

if rospy.get_param("test"):
	# Publish visible percentage from simulation only if we are in test mode (i.e. we are generating optimal plan)
	pub_visible = rospy.Publisher(rospy.get_param("roi_visibility_topic"), Float32, queue_size=1)

##################################
####### SETUP SIMULATION
##################################

def createScene(rootNode):
	print("Python version:", sys.version) 
	rootNode.createObject('RequiredPlugin',name='SofaPython',pluginName='SofaPython')
	rootNode.createObject('BackgroundSetting', color='1 1 1 1')

	print("Waiting to receive the first pose for PSM1...")
	while True:
		if psm1_pose is not None:
			psm1_init = psm1_pose
			Simulation(rootNode, psm1_init)
			return
		else:
			rospy.sleep(0.1)

class Simulation(Sofa.PythonScriptController):

	def __init__(self, rootNode, psm1_init_pose):
		super(Simulation,self).__init__()

		self.tissue = None
		self.psm1_init_pose = psm1_init_pose
		self.rootNode = rootNode

		self.unstable_pub = rospy.Publisher("/unstable_test", Bool, queue_size=1)
		
		# Global sim parameters
		rootNode.findData('dt').value = options['simulation']['dt']
		rootNode.findData('gravity').value = options['simulation']['gravity']

		# Visualization
		self.view_mode = options['simulation']['view_mode']
		if not self.view_mode:
			# Start animation if you are not visualizing
			rootNode.findData('animate').value = True
			rootNode.createObject('VisualStyle', displayFlags='showBehaviorModels showForceFields')
		else:
			rootNode.findData('animate').value = options['simulation']['animate']
			rootNode.createObject('VisualStyle', displayFlags='hideBehaviorModels hideForceFields showCollisionModels')
			rootNode.createObject('LightManager')
			rootNode.createObject('DirectionalLight', name="light1", color="1 1 1", direction="-0.7 0.7 0") 
			rootNode.createObject('DirectionalLight', name="light2", color="1 1 1", direction="0.7 -0.7 0") 
			rootNode.createObject('Visual3DText', text='PRE-OP SIMULATION', position=[0,-0.08,0.06], scale=0.02, color=[0,0,0,1])
	
		# Configuration 			
		num_conf      = rospy.get_param("id_configuration") # number of the chosen configuration
		self.num_conf = num_conf
		rospy.logwarn('VALIDATION EXPERIMENT for configuration '+str(num_conf))

		conf = np.load(options["filenames"]["configurations"]+str(num_conf)+'.npz')
		self.target_position = conf['target']
		self.target_position[1] += 0.0075 # adjust visual target with correct world reference frame

		if "R" in num_conf:
			# We are running real world experiments
			num_bounds = rospy.get_param("real_variant")
			assert num_bounds in ["0", "1", "ground"], "The specified configuration of APs does not exist"
			if num_bounds == "ground":
				self.fixed_indices 	 = conf['ground_APs'].tolist()
				rospy.logwarn('EXPERIMENT for real environment with '+str(num_bounds))
			else:
				self.fixed_indices 	 = conf['APs'][int(num_bounds)]
				rospy.logwarn('EXPERIMENT for real environment, configuration '+str(num_bounds))
		else:
			# We are running simulation experiments
			self.fixed_indices 	 = conf['ground_APs'].tolist()

		self.ks = 0.0
		self.kd = 0.0
		self.createGraph(rootNode)
		return None


	def createGraph(self,rootNode):
		
		# Mesh
		datadir    = options['filenames']['datadir']
		undeformed_surface_mesh = os.path.join(datadir, options['filenames']['surface_mesh'])
		undeformed_tetra_mesh   = os.path.join(datadir, options['filenames']['tetra_mesh'])
		self.undeformed_tetra_mesh = undeformed_tetra_mesh
		ee_surface_mesh = os.path.join(datadir, options['filenames']['ee_mesh'])
		ee2_surface_mesh = os.path.join(datadir, options['filenames']['ee2_mesh'])
		target_surface_mesh = os.path.join(datadir, options['filenames']['target_mesh'])
		self.vtk_target_mesh = utils.loadMesh(target_surface_mesh)
		plane_mesh = os.path.join(datadir, options['filenames']['floor_mesh'])
					
		# Tissue
		material = Material(
			young_modulus=options['simulation']['model']['E'],
			poisson_ratio=options['simulation']['model']['nu'],
			constitutive_model=options['simulation']['model']['material'],
			mass_density=options['simulation']['model']['rho'],
		)

		tissue = Tissue(
				rootNode,
				surface_mesh=undeformed_surface_mesh,
				tetra_mesh=undeformed_tetra_mesh,
				material=material,
				name='Tissue',
				view=True
			)
		self.tissue = tissue
		self.visible_roi_indices = []

		if options['simulation']['model']['material'] == "Corotated":
			self.tissue.compute_von_mises()
	
		if self.view_mode:
			color_lines = [0.8,0.3,0.1,1]
			tissue.surface_node.createObject('MeshTopology', src='@surface_mesh', name='surface')
			tissue.surface_node.createObject('LineCollisionModel', color=color_lines)
		
		########################################################################
		# Target
		target = Target(rootNode, 
						surface_mesh=target_surface_mesh,
						view=True	
					)
		
		# Init position
		target.set_position( self.target_position )	
		self.target = target

		#####################################
		# Fixed points
		fixed_positions = [ self.tissue.tetra_mesh_topology.position[i] for i in self.fixed_indices ]
		print 'Number of fixed points: ', len(self.fixed_indices)
			
		self.boundaries = FixedBoundaries(
				parent_node=tissue.node,
				indices=self.fixed_indices,
				view=self.view_mode
			)
		
		if self.view_mode:
			color_fixed = [0, 0, 1, 1]
			visual_fixed_node = tissue.node.createChild('VisualFixed')
			self.visual_fixed_points = Points( visual_fixed_node,
     									  position=fixed_positions,
										  scale=12,
										  color=color_fixed,
										)
		########################################################################
		# Solver
		solver_name = 'Solver1'
		solver_type = options['simulation']['solver']
		solver = Solver(parent_node=tissue.node, analysis=Solver.DYNAMIC, solver=solver_type, ks=self.ks, kd=self.kd, solver_name=solver_name, num_iter=1000)
		tissue.node.createObject('MeshMatrixMass', massDensity=material.mass_density, name='myMatrixMass')

		########################################################################
		# Psm
		grasp_distance 	   = options['simulation']['grasp_distance']
		num_grasped_points = options['simulation']['num_grasped_points']

		psm1 = EE(rootNode, 
				tissue, 
				name='PSM1', 
				num_grasp_points=num_grasped_points, 
				grasp_distance=grasp_distance,
				surface_mesh=ee_surface_mesh
			)

		psm1.set_position( self.psm1_init_pose )
		self.psm1 = psm1

		psm2 = EE(rootNode, 
				tissue, 
				name='PSM2', 
				num_grasp_points=num_grasped_points, 
				grasp_distance=grasp_distance,
				surface_mesh=ee2_surface_mesh
			)

		self.psm2_init_pose = self.psm1_init_pose
		self.psm2_init_pose[0] += 0.02
		psm2.set_position( self.psm2_init_pose )
		self.psm2 = psm2
		
		# Initialize root bbox for visualization (with qglviewer interface)
		rootNode.bbox = tissue.bounding_box

		########################################################################
		# Plane
		plane = Plane(rootNode,
			surface_mesh=plane_mesh,
			)

		########################################################################
		# Camera
		tf_list.waitForTransform(rospy.get_param("fixed_frame"), "camera_color_optical_frame", time=rospy.Time(0.), timeout = rospy.Duration(5.))
		(t_rgbd, q_rgbd) = tf_list.lookupTransform(rospy.get_param("fixed_frame"), "camera_color_optical_frame", time=rospy.Time(0.))
		rgbd_pose = tq2sofa( t_rgbd, q_rgbd )

		rgbd = Camera(rootNode,
					rgbd_pose,
					name='RGBD'
					)
		self.rgbd = rgbd

		global t_ecm, q_ecm
		if t_ecm is None and q_ecm is None:
			# We are using simulated dVRK, so read ECM position from file
			tf_list.waitForTransform(rospy.get_param("fixed_frame"), "ECM_tip", time=rospy.Time(0.), timeout = rospy.Duration(5.))
			(t_ecm, q_ecm) = tf_list.lookupTransform(rospy.get_param("fixed_frame"), "ECM_tip", time=rospy.Time(0.))

		ecm_pose = tq2sofa( t_ecm, q_ecm )
		ecm = Camera(rootNode,
					ecm_pose,
					name='ECM'
					)
		self.ecm = ecm

		########################################################################
		# Surface points visible from the RGBD camera, if we are on real environment
		if "R" in self.num_conf:
			color_visible = [1, 0, 0, 1]
			tissue_visible_points_node = tissue.node.createChild('TissueVisiblePoints')
			self.tissue_visible_points = Points( tissue_visible_points_node,
     									  position=[0,0,0],
										  scale=4,
										  color=color_visible,
										)
			
		# Target points visible from the ECM camera
		color_visible = [0, 1, 1, 1]
		target_visible_points_node = target.node.createChild('TargetVisiblePoints')
		self.target_visible_points = Points( target_visible_points_node,
     									  position=[0,0,0],
										  scale=4,
										  color=color_visible,
										  view=rospy.get_param("test")
										)
			

	def bwdInitGraph(self, rootNode):
		self.start_time = time.time()

		# Get tissue points within 1cm from the target
		self.visible_roi_indices = self.tissue.compute_spherical_roi( self.target.target_position, 0.01 )
		
		# Initialize full target visibility only if we are in test mode
		if rospy.get_param("test"):
			target_vtk = utils.sofa2vtk( self.target.surface_state, self.target.surface_mesh_loader )
			id_target, __ = utils.get_visible_points( target_vtk, self.ecm )
			self.full_visibility = len( id_target )
			target_visible_points = [ self.target.surface_state.position[i] for i in id_target ]
			self.target_visible_points.update_positions( target_visible_points )
			
		# Publish tissue state at rest
		print 'Sending initial tissue state...'
		publish_pointcloud2( self.tissue.state.position, pub_tissue, rgba_tissue )
		
	def onBeginAnimationStep(self,dt):

		# Setting received pose at each time step
		self.psm1.set_position( psm1_pose )
		self.psm2.set_position( psm2_pose )

		# Setting grasping state at each time step
		self.psm1.has_grasped = psm1_grasp
		self.psm2.has_grasped = psm2_grasp

		# Publish grasp pose only once, at grasping time
		global send_grasp_pose, grasp_pose
		if send_grasp_pose == True:
			if self.psm1.has_grasped:
				grasp_pose = psm1_pose			
			elif self.psm2.has_grasped:
				grasp_pose = psm2_pose
			grasped_roi_indices = self.tissue.compute_spherical_roi( grasp_pose, 0.01 )
			pub_grasped_tissue.publish(Int32MultiArray(data=grasped_roi_indices))
			send_grasp_pose = False

		# Update attachment points if required
		if do_update > 0 and len(banet_indices):
			rospy.loginfo('Triggered update, banet predicted points: '+str(len(banet_indices)))
			self.tissue.reset_positions( banet_indices )
			self.boundaries.node.indices = banet_indices
			new_fixed_positions = [ self.tissue.state.position[i] for i in banet_indices ]
			self.fixed_indices = banet_indices
			if self.view_mode:
				self.visual_fixed_points.update_positions( new_fixed_positions )
		else:
			# Force fixed points to avoid unexpected behaviors
			self.tissue.reset_positions( self.fixed_indices )
			self.boundaries.node.indices = self.fixed_indices
		

	def onEndAnimationStep(self,dt):
		
		# Tissue kept at rest state if psms are not grasping
		if not(self.psm1.has_grasped) and not(self.psm2.has_grasped):	
			self.reset()
			global grasp_pose
			grasp_pose = None

		# Check for simulation instability at the end of each time step
		if self.tissue.is_stable is False:
			print("Simulation is UNSTABLE")
			self.unstable_pub.publish(Bool(True))
		
		# Convert target and tissue to vtk
		tissue_vtk = utils.sofa2vtk( self.tissue.surface_state, self.tissue.surface_mesh_loader )
		target_vtk = utils.sofa2vtk( self.target.surface_state, self.target.surface_mesh_loader )

		# Visible surface points from RGBD, if we are in real environment
		if "R" in self.num_conf:
			id_tissue, __ = utils.get_visible_points( tissue_vtk, self.rgbd )
			tissue_visible_points = [ self.tissue.surface_state.position[i] for i in id_tissue ]
			self.tissue_visible_points.update_positions( tissue_visible_points )

		# Compute visible target percentage only if we are in test mode
		if rospy.get_param("test"):
			tissue_roi   = [ self.tissue.state.position[i] for i in self.visible_roi_indices ]
			tissue_roi_y = np.mean( np.asarray(tissue_roi), axis=0 )[1]
			# If target is above tissue (due to failed/missing collision)
			if self.target_position[1] >= tissue_roi_y:
				id_target = []
			else:
				# Compute visible points from ECM
				id_target, __ = utils.get_visible_points( target_vtk, self.ecm, obstacle=tissue_vtk )

			target_visible_points = [ self.target.surface_state.position[i] for i in id_target ]
			self.target_visible_points.update_positions( target_visible_points )

			num_visible_roi = len(id_target)
			visible_perc = float(num_visible_roi) / self.full_visibility
			# print "Visible %: ", visible_perc*100
			pub_visible.publish(Float32(visible_perc*100))
		
		#########################################################

		# Publish tissue state
		if "R" in self.num_conf:
			publish_pointcloud2( self.tissue_visible_points.state.position, pub_tissue, rgba_tissue )
		else:
			publish_pointcloud2( self.tissue.state.position, pub_tissue, rgba_tissue )
		
		# Publish forces
		vonMisesPerNode = np.asarray(self.tissue.fem.vonMisesPerNode)
		pub_stress.publish(Float32(np.amax(vonMisesPerNode)))
		
		forces = [ f for sublist in self.tissue.state.force for f in sublist ]

		msg_forces = Float32MultiArray(data=forces)
		pub_forces.publish(msg_forces)

		# Publish fixed points
		fixed_points = [ self.tissue.state.position[i] for i in self.fixed_indices ]
		publish_pointcloud2( fixed_points, pub_fixed, rgba_fixed )

		# Publish target position
		target_position = position2world(self.target.target_position)
		msg_target = Float32MultiArray(data=target_position[0])
		pub_target.publish(msg_target)
		
	def reset(self):
		self.tissue.reset()
		sys.stdout.flush()
		return 0


def publish_pointcloud2(positions, topic, rgba):
	points = []
	for x,y,z in positions:
		x,y,z,__ = list(tf_sofa2world.dot(np.asarray([x,y,z,1])))
		points.append([x,y,z,rgba])
	
	header = Header()
	header.frame_id = rospy.get_param("fixed_frame")
	header.stamp = rospy.Time.now()
	pcl_msg = point_cloud2.create_cloud(header, fields, points)
	topic.publish(pcl_msg)

def position2world(positions):
	points = []
	for x,y,z in positions:
		x,y,z,__ = list(tf_sofa2world.dot(np.asarray([x,y,z,1])))
		points.append([x,y,z])
	return points

def tq2sofa(transl, quat):
	# Create 4x4 transformation matrix
	rotation = R.from_quat(quat)
	r_matrix = rotation.as_dcm()
	tf = np.vstack( (r_matrix, np.zeros((1,3)) ) )
	transl.append(1)
	tf = np.hstack( (tf, np.asarray(transl).reshape(-1,1)) )
	# Convert to sofa
	tf_sofa = tf_world2sofa.dot(tf)
	# Come back to transl and quat
	transl_sofa = tf_sofa[:3,3]
	rot_sofa 	= tf_sofa[:3,:3]
	rotation 	= R.from_dcm(rot_sofa)
	quat_sofa 	= rotation.as_quat()

	transl_sofa = transl_sofa.tolist()
	quat_sofa 	= quat_sofa.tolist()
	transl_sofa.extend(quat_sofa)
	camera_pose = transl_sofa
	return camera_pose
