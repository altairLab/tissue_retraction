import numpy as np
import time
import random
import sofa_utils
import components as comp

class Target():
	"""
	Mechanical object defining the target position.
	"""
	# SOFA components
	node    = None 
	state   = None

	#########################################################
	####### CREATION
	#########################################################
	def __init__(self, parent_node, **kwargs):
		""" 
		Keyword arguments:
			parent_node (Sofa Node):
				parent node to which the object is attached.
			view (bool): 
				If true, the target is visualized (default True).
			surface_mesh (str): 
				Path to the surface mesh of the simulated object (default None).
			name (str):  
				Name of the node where the object is created (default 'SofaObject').
		"""
		self.parent_node = parent_node
		self.view = kwargs.get('view', True)
		self.surface_mesh = kwargs.get('surface_mesh', None)
		self.node_name = kwargs.get('name', 'Target')

		# creates the graph
		self.__create_sofa_graph()

	def __create_sofa_graph(self):
		target_node = self.parent_node.createChild(self.node_name)
		
		# Mechanical object
		self.state = target_node.createObject('MechanicalObject', position=[0,0,0,0,0,0,1], name=self.node_name+'_state', template='Rigid3d',showObject=False)
		self.target_position = [[0,0,0]]

		self.__visualize_mechanical()
		self.node = target_node

		# Surface mesh
		if self.surface_mesh:
			surface_node_name = 'Visual'+self.node_name
			surface_node = self.node.createChild(surface_node_name)
			
			surface_mesh_name = 'surface_mesh'
			self.surface_mesh_loader = comp.load_mesh(surface_node,filename=self.surface_mesh,node_name=surface_mesh_name)
			self.surface_state = surface_node.createObject('MechanicalObject', src='@surface_mesh', name=surface_node_name+'_state', showObject=False)
			surface_node.createObject('RigidMapping')
			self.surface_node = surface_node

			# Visualization
			if self.view:
				self.visualize_surface()

	#########################################################
	####### CUSTOM
	#########################################################
	def set_position( self, position ):
		if not isinstance(position, list):
			position = list(position)
		self.__update_position(position)
		return position
	
	def set_rnd_position( self, bounds=[0,0,0,1,1,1], seed=None ):
		# Bounds are expressed as [xmin, ymin, zmin, xmax, ymax, zmax]
		if seed is None:
			seed = time.time()
		random.seed(seed)
		xt = random.uniform(bounds[0], bounds[0+3])
		yt = random.uniform(bounds[1], bounds[1+3])
		zt = random.uniform(bounds[2], bounds[2+3])
		new_position = [xt, yt, zt]
		self.__update_position(new_position)
		return new_position

	def visualize_surface( self, color=[1, 0, 1, 0.7] ):
		visual_node = self.surface_node.createChild('VisualSurface')
		visual_node.createObject('VisualStyle', displayFlags='showWireframe')
		visual_node.createObject('OglModel', name="VisualSurfaceOGL", src="@../surface_mesh", color=color, listening=1)
		visual_node.createObject('IdentityMapping')

	#########################################################
	####### CUSTOM - private
	#########################################################
	def __update_position( self, position ):
		self.target_position = [position]

		new_position = sofa_utils.convert2rigid( position )
		self.state.position  = new_position
		if self.surface_mesh:
			self.surface_state.position = new_position

	def __visualize_mechanical( self, scale=0.002, color=[1,1,1,1] ):
		self.state.showObject=1
		self.state.showObjectScale=scale
		self.state.showColor=color