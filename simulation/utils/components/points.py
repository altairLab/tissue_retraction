
class Points():
	"""
	Mechanical object containing only DOFs, usually.
	"""
	# SOFA components
	state   = None

	#########################################################
	####### CREATION
	#########################################################
	def __init__(self, parent_node, **kwargs):
		""" 
		Keyword arguments:
			parent_node (Sofa Node):
				parent node to which the object is attached.
			name (str):  
				Name of the mechanical object (default 'MechanicalObject').
			position (list):
				Coordinates of the mechanical object positions.
			scale (int):
				Dimension of the points.
			color (list):
				RGBDA of the points.
		"""
		self.parent_node  = parent_node
		self.node_name 	  = kwargs.get('name', 'MechanicalObject')
		self.position	  = kwargs.get('position', [0,0,0])
		self.scale 		  = kwargs.get('scale', 2)
		self.color 		  = kwargs.get('color', [1,1,1,1])
		self.view 		  = kwargs.get('view', True)

		# creates the graph
		self.__create_sofa_graph()

	def __create_sofa_graph(self):
		# Mechanical object
		self.state = self.parent_node.createObject('MechanicalObject', name=self.node_name, position=self.position, showObject=self.view, showObjectScale=self.scale, showColor=self.color, listening=1)
		
	#########################################################
	####### CUSTOM
	#########################################################
	def update_positions(self, new_position):
		self.state.size     = len(new_position)
		self.state.position = new_position

		reset_list = [ [0,0,0] for i in range(len(new_position)) ]
		self.state.velocity       = reset_list
		self.state.force          = reset_list
		self.state.rest_position  = reset_list
		self.state.reset_position = reset_list
		self.state.derivX         = reset_list

