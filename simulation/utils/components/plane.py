import os, math, sys
import numpy as np
import Sofa

import components as comp

class Plane(Sofa.PythonScriptController):
	"""
	Mechanical object describing a rigid object which can be associated to a collision model.
	"""
	# SOFA components
	node    = None 
	state   = None

	#########################################################
	####### CREATION
	#########################################################
	def __init__(self, parent_node, **kwargs):
		""" 
		Keyword arguments:
			parent_node (Sofa Node):
				parent node to which the object is attached.
			surface_mesh (str): 
				Path to the surface mesh of the simulated object (default None).
			name (str):  
				Name of the node where the object is created (default 'Plane').
			collision (bool):
				if True, collision model is associated to the object.
		"""
		self.parent_node  = parent_node
		self.surface_mesh = kwargs.get('surface_mesh', None)
		self.color 		  = kwargs.get('color', [0.6,0.7,0.7,1])
		self.node_name 	  = kwargs.get('name', 'Plane')
		self.collision    = kwargs.get('collision', False)

		# creates the graph
		self.__create_sofa_graph()

	def __create_sofa_graph(self):
		plane_node = self.parent_node.createChild(self.node_name)

		surface_mesh_name = 'surface_mesh'
		comp.load_mesh(plane_node,filename=self.surface_mesh,node_name=surface_mesh_name)
		plane_node.createObject('MeshTopology', src='@surface_mesh', name=surface_mesh_name+'_topology')
		
		# Mechanical object
		self.state = plane_node.createObject('MechanicalObject', name=self.node_name+'_state', showObject=False)
			
		if self.collision:
			#plane_node.createObject('UniformMass',totalMass='0.2',name='plane_mass',showAxisSizeFactor='0')
			comp.create_collision_models(plane_node)
		
		self.node = plane_node
		self.__visualize_surface()

	#########################################################
	####### CUSTOM - private
	#########################################################
	def __visualize_surface( self ):
		visual_node = self.node.createChild('VisualSurface')
		visual_node.createObject('VisualStyle', displayFlags='showVisual hideWireframe')
		visual_node.createObject('OglModel', name="VisualSurfaceOGL", src="@../surface_mesh", color=self.color)
		visual_node.createObject('IdentityMapping')

