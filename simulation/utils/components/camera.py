import Sofa
import numpy as np
from scipy import spatial
import math
import sofa_utils

class Camera(Sofa.PythonScriptController):
	"""
	Oriented rigid object defining a camera.
	"""
	# SOFA components
	node 		= None
	state 		= None

	#########################################################
	####### CREATION
	#########################################################
	def __init__(self, parent_node, pose, **kwargs):
		""" 
		parent_node: parent node for the end effector
		pose: 3D coordinates of the camera and orientation of the camera (as quaternion)
		Keyword arguments:
			name (str):  
				Name of the node where the object is created (default 'camera').
		"""
		self.parent_node = parent_node
		self.pose 	 	 = pose
		self.node_name 	 = kwargs.get('name', 'camera')
		
		self.__create_sofa_graph()

	def __create_sofa_graph( self ):
		if not isinstance(self.pose, list):
			self.pose = list(self.pose)

		camera_node = self.parent_node.createChild(self.node_name)
		self.state = camera_node.createObject('MechanicalObject',name=self.node_name, position=self.pose, template='Rigid3d', showObject=1, showObjectScale=0.005)
		self.node = camera_node
	