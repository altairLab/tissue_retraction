import Sofa
import numpy as np
from scipy import spatial
import math
import sofa_utils
import components as comp

class EE(Sofa.PythonScriptController):
	"""
	Mechanical object describing a robot end-effector.
	"""
	# SOFA components
	node 		= None
	state 		= None
	ee_springs 	= None 
	tissue	    = None
	visual_state = None

	# EE state
	has_grasped = False #changes when grasping is triggered from ROS
	is_close 	= False #changes when tissue is close enough to the EE
	is_moving   = False
	time 	    = 0
	motion_time = -1 

	#########################################################
	####### CREATION
	#########################################################
	def __init__(self, parent_node, tissue, **kwargs):
		""" 
		parent_node: parent node for the end effector
		tissue: class of the tissue with which the end effector can grasp
		Keyword arguments:
			name (str):  
				Name of the node where the object is created (default 'EE').
		"""
		self.parent_node = parent_node
		self.tissue = tissue
		self.node_name = kwargs.get('name', 'EE')
		self.num_grasp_points = kwargs.get('num_grasp_points', 1)
		self.grasp_distance = kwargs.get('grasp_distance', None)
		self.surface_mesh = kwargs.get('surface_mesh', None)
		
		if self.grasp_distance is None:
			# if we do not specify a grasp distance, grasping is always triggered regardless of ee-tissue distance
			self.is_close = True 

		self.__create_sofa_graph()

	def __create_sofa_graph( self ):
		# Define EE mechanical state
		ee_indices   = list(range(self.num_grasp_points))
		ee_positions = [0.0,0.0,0.0]*self.num_grasp_points

		ee_node = self.parent_node.createChild(self.node_name)
		self.state = ee_node.createObject('MechanicalObject',name=self.node_name, position=ee_positions, template='Vec3d', showObject=0)
		ee_link = self.state.getLinkPath()

		# Define stiff springs to handle grasping with tissue
		springs_name = self.node_name+'_attach'
		self.ee_springs = self.tissue.node.createObject('RestShapeSpringsForceField', name=springs_name,
						external_rest_shape=ee_link,drawSpring=0,
						stiffness=0, angularStiffness=0,listening=1,
						points=ee_indices, external_points=ee_indices)

		self.node = ee_node

		if not self.surface_mesh is None:
			self.visualize_surface()
			
	#########################################################
	####### SOFA-RELATED
	#########################################################
	def onBeginAnimationStep(self,dt):
		# Check if grasping position is met
		self.__check_grasping()

		# Check if we exceeded motion time
		if self.time >= self.motion_time:
			self.is_moving = False
			self.motion_time = -1
			self.time = 0

		# Update motion if EE is moving
		if self.is_moving:
			self.__update_position()
			self.time += dt
	
	def reset(self):
		pass
		#self.state.position = self.state.rest_position
		#self.__set_visual_position( self.state.position )
	
	#########################################################
	####### CUSTOM
	#########################################################
	def set_position( self, new_position ):
		if not isinstance(new_position, list):
			new_position = list(new_position)
		new_position = new_position*self.num_grasp_points
		self.state.position = new_position
		self.__set_visual_position( new_position )

   	def create_motion( self, final_position, dt, velocity, single_step=False ):
		"""
		Creates movement to displace EE from its current position to the final position
		provided, at the given velocity
 		"""
		current_position = self.state.position
		displacement = np.asarray(final_position) - np.asarray(current_position)
		# Check if displacement is too small
		if single_step:
			print('Applying the motion in a single step, velocity will be neglected.')
			self.motion_time = dt
		else:
			self.motion_time = math.ceil( np.linalg.norm( displacement ) / velocity )
		self.motion_delta = displacement * dt / self.motion_time
		self.is_moving = True
	
	def release_grasp( self ):
		# Releases the tissue, in case it has been grasped
		if self.has_grasped:
			self.ee_springs.stiffness = 0
			self.ee_springs.angularStiffness = 0
			self.has_grasped = False

	def visualize_surface( self ):
		visual_ee_position = sofa_utils.convert2rigid( self.state.position )
		node_name = 'VisualNode'
		visual_ee_node = self.node.createChild(node_name) 
		self.visual_state = visual_ee_node.createObject('MechanicalObject',name='VisualEE', position=visual_ee_position, template='Rigid3d', showObject=0)

		visual_ee_mesh_node = visual_ee_node.createChild('Visual')
		surface_mesh_name = 'EEloader'
		comp.load_mesh(visual_ee_mesh_node,filename=self.surface_mesh,node_name=surface_mesh_name)	
		visual_ee_mesh_node.createObject('TriangleSetTopologyContainer', name='EEtopology', src='@'+surface_mesh_name)
		visual_ee_mesh_node.createObject('OglModel', color=[0.5, 0.5, 0.5, 1])
		visual_ee_mesh_node.createObject('RigidMapping')

	#########################################################
	####### CUSTOM - private
	#########################################################
	def __set_visual_position( self, position ):
		if self.visual_state is not None:
			visual_position = sofa_utils.convert2rigid( position )
			self.visual_state.position = visual_position

	def __update_position( self ):
		current_position = np.asarray(self.state.position)
		updated_position = current_position + self.motion_delta			
		self.state.position = updated_position.tolist()
		self.__set_visual_position( updated_position )

	def __check_grasping( self ):
		tissue_state = np.asarray( self.tissue.state.position )
		ee_state = (np.asarray( self.state.position[0] )).reshape((1,-1))

		distances = spatial.distance.cdist(tissue_state, ee_state)

		if self.grasp_distance is not None and np.amin(distances) <= self.grasp_distance:
			self.is_close = True
		
		if self.has_grasped and self.is_close:
			#print self.node_name, 'GRASPING'
			# Select the required number of points closest to the end effector
			distances = np.ravel(distances)
			grasped_indices = np.argpartition(distances, self.num_grasp_points)
			grasped_indices = grasped_indices[:self.num_grasp_points].tolist()
			self.ee_springs.points = grasped_indices
			self.ee_springs.stiffness = 500
			self.ee_springs.angularStiffness = 5000
		else:
			self.ee_springs.stiffness = 0
			self.ee_springs.angularStiffness = 0
			
	def __visualize_mechanical( self, scale=10, color=[0,1,0,1] ):
		self.state.showObject=1
		self.state.showObjectScale=scale
		self.state.showColor=color