"""
Generates configurations of fixed points for real environment and saves them as a .npz file.
Regions of fixed points are generated as composition of bounding boxes defined in the file bbox_config_real.yml
"""
import sys, os
sim_dir_path = os.path.split(os.path.abspath(__file__))[0]
sim_dir_path = sim_dir_path+"/../"
import numpy as np
import math
from vtk import *

sys.path.append( os.path.join(sim_dir_path, 'utils') )
import utils

# Load config file
config_file = './config/settings.yml'
config_file = os.path.join(sim_dir_path, config_file)
if len(sys.argv) > 1:
	config_file = sys.argv[1]
options = utils.load_yaml(config_file)
print "Loaded config from ", config_file

# Mesh
datadir    = os.path.join(sim_dir_path, options['filenames']['datadir'])
undeformed_surface_mesh = os.path.join(datadir, options['filenames']['surface_mesh'])
undeformed_tetra_mesh   = os.path.join(datadir, options['filenames']['tetra_mesh'])
vtk_tetra_mesh = utils.loadMesh( undeformed_tetra_mesh )

# Boundaries definition, read from yml file
boundariesFile = os.path.join( './bbox_config_real.yml' )
boundaries = utils.load_yaml(boundariesFile)

def generate_configurations():

	for c in range(len(boundaries)): 
		keys = boundaries[c].keys()
		
		# Init output dict
		configurations = {}
		configurations['APs'] = []

		# Init output mesh
		curr_volume_mesh = vtkUnstructuredGrid()
		curr_volume_mesh.DeepCopy(vtk_tetra_mesh)

		for k in keys:

			if k == 'target':
				configurations['target'] = boundaries[c][k]
				target_closest_id = closest_ind_to_target(curr_volume_mesh, boundaries[c][k])
				add_array(curr_volume_mesh, [target_closest_id], 'target')
			else:
				bboxes = boundaries[c][k]

				# Get indices for all the specified bboxes
				fixedPointsIndices = []
				for nbox in range(len(bboxes)):
					fixedBbox = tuple( boundaries[c][k][nbox] )

					roi0 = vtkPointOccupancyFilter()
					roi0.SetInputData(vtk_tetra_mesh)
					roi0.SetModelBounds(fixedBbox)

					points0 = vtkMaskPointsFilter()
					points0.SetInputData(vtk_tetra_mesh)
					points0.SetMaskConnection(roi0.GetOutputPort())
					points0.GenerateVerticesOn()
					points0.Update()
					fixedPoints0 = points0.GetOutput()

					fixedPointsIndices0 = utils.getClosestPoints( fixedPoints0, vtk_tetra_mesh )

					fixedPointsIndices = fixedPointsIndices + fixedPointsIndices0

					del roi0, points0, fixedPoints0, fixedPointsIndices0

				if k == 'ground':
					configurations[k] = fixedPointsIndices
					add_array(curr_volume_mesh, fixedPointsIndices, 'ground')
				else:
					configurations['APs'].append(fixedPointsIndices)
					add_array(curr_volume_mesh, fixedPointsIndices, 'AP'+str(k))

		# Output files		
		vtk_filename = os.path.join(os.getcwd(), 'configurations/confR'+str(c)+'.vtk')
		write_file(vtk_filename, curr_volume_mesh)

		output_filename = os.path.join(os.getcwd(), 'configurations/confR'+str(c)+'.npz')
		np.savez_compressed(output_filename, target=configurations['target'], APs=configurations['APs'], ground_APs=configurations['ground'])

# Defs for output writing
def closest_ind_to_target(mesh, target):
	locator = vtkPointLocator( )
	locator.SetDataSet( mesh )
	locator.SetNumberOfPointsPerBucket(1)
	locator.BuildLocator()

	closestMeshPointID = locator.FindClosestPoint( target )
	return int(closestMeshPointID)

def add_array(mesh, array, array_name):
	newArray = vtkDoubleArray()
	newArray.SetNumberOfComponents(1)
	newArray.SetName(array_name)
	newArray.SetNumberOfTuples(mesh.GetNumberOfPoints())
	newArray.Fill(0)
	for k in array:
		newArray.SetTuple1(k, 1)
	mesh.GetPointData().AddArray( newArray )
	return mesh

def write_file(filename, mesh):
	print "Writing to ", filename
	writer = vtkUnstructuredGridWriter()
	writer.SetFileName( filename )
	writer.SetInputData( mesh )
	writer.Update()

if __name__ == "__main__":
	generate_configurations()
