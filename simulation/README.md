# Simulation
This directory contains the simulation which exchanges information about tissue state with ROS. 
The folder contains the following files: 
- `simulation.py` represents the *simulated environment* (or *pre-operative simulation*). It is the main simulation file and is initialized with pre-operative information of APs. Such APs are updated during execution in case *Learning* is triggered by *Monitoring* module;
- `actual_simulation.py` represents the *actual simulation*. It is used to mimick the real environment for the simulation experiments.
- data/ contains 3d mesh models
- scripts/ contains the .npz files with the configurations used and an example script to generate such files (used to create real world configurations)
- utils/ contains custom sofa components and utility functions
- config/ contains configuration file defining simulation properties

Simulation scenes can be run by directly calling `runSofa simulation.py`)

### Setup
Download and install SOFA (v20.06), following the instructions at https://www.sofa-framework.org/community/doc/getting-started/build/linux/. Simulation is implemented relying on SofaPython plugin, which needs to be manually activated in CMake (thus, python3 is not supported). Once built, SOFA can be launched with /PATH-TO-BUILD/bin/runSofa (you can either add runSofa to your ~/.bashrc or create a symlink). Required python modules for simulations can be found in simulation/requirements.txt.

Communication is implemented using ROS. ROS-related modules come with ROS distributions (tested with ROS melodic).
