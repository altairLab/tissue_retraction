scipy
numpy
copy
vtk==8.2.0
PyYAML
time
plyfile
robust_laplacian
torch==1.5.0
