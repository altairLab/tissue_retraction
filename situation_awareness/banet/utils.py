from vtk import *
import math
import numpy as np
from numpy import linalg as LA
from scipy import spatial

def getBoundariesOnGrid(grid_vtk, stiffness):
	# Returns list of points associated to non zero stiffness
	stiffnessNonZero = np.ravel(np.nonzero(stiffness))
	gridBoundaries = np.asarray([])
	for i in stiffnessNonZero:
		gridBoundaries = np.append(gridBoundaries, grid_vtk.GetPoint(i))
	
	# if binary:
	return gridBoundaries.tolist()
	# else:
		# stiffness = stiffnessRaw[stiffnessNonZero]
		# return gridBoundaries.tolist(), stiffness.tolist()

# VTK-RELATED UTILS
def get_centering_transform( mesh ):
	bounds = [0]*6
	mesh.GetBounds(bounds)
	tf = vtkTransform()
	dx = -(bounds[1]+bounds[0])*0.5
	dy = -(bounds[3]+bounds[2])*0.5
	dz = -(bounds[5]+bounds[4])*0.5
	tf.Translate( (dx,dy,dz) )
	return tf, dx, dy, dz

def get_vtk_transform( matrix_list ):
	tf = vtkTransform()
	tf.SetMatrix( matrix_list )
	return tf

def apply_vtk_transform( mesh, transform ):
	tfFilter = vtkTransformFilter()
	tfFilter.SetTransform( transform )
	tfFilter.SetInputData( mesh )
	tfFilter.Update()
	return tfFilter.GetOutput()

def numpy2polydata( vertices ):
	pts = vtkPoints()
	verts = vtkCellArray()
	for index, row in enumerate(vertices):
		x,y,z = row
		pts.InsertNextPoint(x,y,z)
		verts.InsertNextCell(1, (index,))
	outpoly = vtkPolyData()
	outpoly.SetPoints(pts)
	outpoly.SetVerts(verts)
	return outpoly

def compute_warp_displacement( mesh, pcd, mesh_indices, min_visible_displacement=5e-3, invisible=False ):
	'''
	Given a mesh and a point cloud, associates each mesh point with the 
	corresponding point in the pcd based on the correspondences identified by mesh_indices
	and computes the displacement vector. The mesh is assumed to be originally flat, with
	flat surface normal to y direction. This association is important for displacement
	calculation of invisible points (which is based only on x and z coordinates, and not on y).

	Parameters
	----------
	mesh (vtkPolyData):
		Starting mesh whose vertices have to be associated with a displacement vector.
	pcd (vtkPolyData):
		Deformed view of the mesh, as a point cloud. 
	mesh_indices (list of ints):
		For each pcd point, contains the corresponding index of the mesh. 
	min_visible_displacement (float):
		If the mesh point is associated to a pcd point, but the magnitude of the computed displacement is
		lower than min_visible_displacement, the corresponding displacement vector is clipped to (1e-05, 1e-05, 1e-05).
		Default: 5e-3
	invisible (bool):
		If True, the displacement vector is computed for all the mesh points (both visible and invisible). For each of the 
		invisible points, the associated displacement is that of the closest visible point (neglecting y coordinate). 
		Default: False

	Returns
	----------
	vtkFloatArray:
		Nx3 displacement vector of each mesh point. 
	'''

	# print("Calculating displacement of visible points" )
	displacement = vtkFloatArray()
	displacement.SetName( "displacement" )
	displacement.SetNumberOfComponents( 3 )
	displacement.SetNumberOfTuples( mesh.GetNumberOfPoints() )
	displacement.Fill(0.0)

	#################################################
	## Calculate input displacement at visible points
	# Keep only closest correspondences
	distance_vector = np.array([np.nan]* mesh.GetNumberOfPoints())
	vol_points = vtkPoints()

	for idx_intraop, idx_vol in enumerate( mesh_indices ):
		p1 = mesh.GetPoint( idx_vol )
		p2 = pcd.GetPoint( idx_intraop )
		displ = (p2[0]-p1[0], p2[1]-p1[1], p2[2]-p1[2])

		distance = np.linalg.norm( np.asarray( displ ) )
		# Replace the displacement...
		# ...if it has not been computed yet
		if distance > min_visible_displacement:
			if np.isnan(distance_vector[idx_vol]):
				distance_vector[idx_vol] = distance
				displacement.SetTuple3( idx_vol, displ[0], displ[1], displ[2] )
			# ...or if distance is lower than the previous
			elif distance < distance_vector[idx_vol]:
				distance_vector[idx_vol] = distance
				displacement.SetTuple3( idx_vol, displ[0], displ[1], displ[2] )
		else:
			# The displacement is almost null, but the area is visible.
			distance_vector[idx_vol] = distance
			displacement.SetTuple3( idx_vol, 1e-05, 1e-05, 1e-05 )

		# Copy point coordinates vol_points_list, neglecting y
		vol_points.InsertNextPoint( p1[0], 0.0, p1[2] )
	
	#################################################
	## Associate displacement to invisible points
	if invisible:
		vol_points_poly = vtkPolyData()
		vol_points_poly.SetPoints( vol_points )

		locator = vtkPointLocator( )
		locator.SetDataSet( vol_points_poly )
		locator.SetNumberOfPointsPerBucket(1)
		locator.BuildLocator()

		# For those idx with associated nan distance, get closest point (in x and z) with not nan distance
		# and associate the same displacement
		for j in range( mesh.GetNumberOfPoints() ):
			if np.isnan( distance_vector[j] ):
				point = mesh.GetPoint(j)
				y_point = ( point[0], 0.0, point[2] )

				ind_min_y_diff = locator.FindClosestPoint( y_point )

				#ind_min_y_diff = np.argmin( np.abs( y_vol_points_list - y ) )
				closestYInd = mesh_indices[ ind_min_y_diff ]
				p_displ = displacement.GetTuple3( closestYInd )
				displacement.SetTuple3( j, p_displ[0], p_displ[1], p_displ[2] )
				distance_vector[j] = np.copy( distance_vector[closestYInd] )

	return displacement

# VTKUTILS
def loadMesh( filename ):
	"""
	Loads a mesh using VTK. Supported file types: stl, ply, obj, vtk, vtu, vtp, pcd.

	Arguments:
	---------
	filename (str)
	
	Returns:
	--------
	vtkDataSet
		which is a vtkUnstructuredGrid or vtkPolyData, depending on the file type of the mesh.
	"""

	# Load the input mesh:
	fileType = filename[-4:].lower()
	if fileType == ".stl":
		reader = vtkSTLReader()
		reader.SetFileName( filename )
		reader.Update()
		mesh = reader.GetOutput()
	elif fileType == ".obj":
		reader = vtkOBJReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".ply":
		reader = vtkPLYReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".vtk":
		reader = vtkUnstructuredGridReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".vtu":
		reader = vtkXMLUnstructuredGridReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".vtp":
		reader = vtkXMLPolyDataReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".pcd":
		print('DISABLED')
		exit()

	else:
		raise IOError("Mesh should be .vtk, .vtu, .vtp, .obj, .stl, .ply or .pcd file!")

	if mesh.GetNumberOfPoints() == 0:
		raise IOError("Could not load a valid mesh from {}".format(filename))
	return mesh

def writeMesh( mesh, filename ):
	"""
	Saves a VTK mesh to file. 
	Supported file types: stl, ply, obj, vtk, vtu, vtp, pcd.

	Arguments:
	---------
	mesh (vtkDataSet):
		mesh to save
	filename (str): 
		name of the file where to save the input mesh. MUST contain the desired extension.
	
	"""

	if mesh.GetNumberOfPoints() == 0:
		raise IOError("Input mesh has no points!")

	# Get file format
	fileType = filename[-4:].lower()
	if fileType == ".stl":
		writer = vtkSTLWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update()
	elif fileType == ".obj":
		writer = vtkOBJWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".ply":
		writer = vtkPLYWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".vtk":
		writer = vtkUnstructuredGridWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".vtu":
		writer = vtkXMLUnstructuredGridWriter() 
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".vts":
		writer = vtkXMLStructuredGridWriter() 
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".vtp":
		writer = vtkXMLPolyDataWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	else:
		raise IOError("Supported extensions are .vtk, .vtu, .vts, .vtp, .obj, .stl, .ply!")

def unstructuredGridToPolyData( ug ):
	"""
	Converts an input unstructured grid into a polydata object. 
	Be careful since PolyData objects cannot contain 3D elements, thus all 
	tetrahedra will be lost with this operation.

	Parameters
	----------
	ug (vtkUnstructuredGrid):
		The input unstructured grid

	Returns
	----------
	vtkPolyData
	
	"""
	geometryFilter = vtkGeometryFilter()
	geometryFilter.SetInputData( ug )
	geometryFilter.Update()
	return geometryFilter.GetOutput()

def getClosestPoints( mesh1, mesh2, subset=None, discardDuplicate=False ):
	"""
	For each point in mesh1, returns the index of the closest point in mesh2. 
	
	Parameters
	----------
	mesh1 (vtkDataSet):
		Topology of the first mesh
	mesh2 (vtkDataSet):
		Topology of the second mesh
	subset (list of ints):
		If specified, a subset of mesh1 nodes are considered. Subset represents
		the list of node indices to consider.
		Default: None
	discardDuplicate (bool):
		If true, the returned indices cannot be present more than once.
		Default: False

	Returns
	----------
	list of int:
		IDs of mesh2 vertices closest to each mesh1 vertex.

	"""
	locator = vtkPointLocator( )
	locator.SetDataSet( mesh2 )
	locator.SetNumberOfPointsPerBucket(1)
	locator.BuildLocator()

	mesh2IDs = []
	if subset is None:
		subset = range(mesh1.GetNumberOfPoints())
		
	for idx in subset:
		mesh1Point = mesh1.GetPoint(idx)
		mesh2PointID = locator.FindClosestPoint( mesh1Point )
		# If we want to discard duplicate indices and we have already found it, skip append
		if discardDuplicate and (mesh2PointID in mesh2IDs):
			pass
		else:
			mesh2IDs.append( mesh2PointID )
	return mesh2IDs

def loadTransformationMatrix( grid ):
	matArray = grid.GetFieldData().GetArray( "TransformationMatrix" )
	if matArray:
		tf = vtkTransform()
		mat = vtkMatrix4x4()
		for i in range( 0, 16 ):
			val = matArray.GetTuple1( i )
			mat.SetElement( int(i/4), i % 4, val )
		tf.SetMatrix( mat )
		return tf
	else:
		raise IOError("No 'TransformationMatrix' array found in field data.")


# VOXELIZE
def createGrid( size, gridSize ):
	grid = vtkStructuredGrid()
	grid.SetDimensions( (gridSize,gridSize,gridSize) )
	points = vtkPoints()
	points.SetNumberOfPoints( gridSize**3 )
	pID = 0
	start = -size/2
	d = size/(gridSize-1)
	for i in range( 0, gridSize ):
		for j in range( 0, gridSize ):
			for k in range( 0, gridSize ):
				x = start + d*k
				y = start + d*j
				z = start + d*i
				points.SetPoint( pID, x, y, z )
				pID += 1
	grid.SetPoints( points )
	return grid

def storeTransformationMatrix( grid, tf ):
	mat = tf.GetMatrix()
	matArray = vtkFloatArray()
	matArray.SetNumberOfTuples(16)
	matArray.SetNumberOfComponents(1)
	matArray.SetName( "TransformationMatrix" )
	for row in range(0,4):
		for col in range(0,4):
			matArray.SetTuple1( row*4+col, mat.GetElement( row, col ) )
	grid.GetFieldData().AddArray(matArray)

def distanceField( surfaceMesh, targetGrid, targetArrayName, signed=False ):
	# Initialize distance field:
	df = vtkFloatArray()
	df.SetNumberOfTuples( targetGrid.GetNumberOfPoints() )
	df.SetName(targetArrayName)

	# Data structure to quickly find cells:
	cellLocator = vtkCellLocator()
	cellLocator.SetDataSet( surfaceMesh )
	cellLocator.BuildLocator()


	for i in range(0, targetGrid.GetNumberOfPoints() ):
		# Take a point from the target...
		testPoint = [0]*3
		targetGrid.GetPoint( i, testPoint )
		# ... find the point in the surface closest to it
		cID, subID, dist2 = mutable(0), mutable(0), mutable(0.0)
		closestPoint = [0]*3
		
		cellLocator.FindClosestPoint( testPoint, closestPoint, cID, subID, dist2 )
		
		dist = math.sqrt(dist2)
		
		df.SetTuple1( i, dist )

	if signed:
		pts = vtkPolyData()
		pts.SetPoints( targetGrid.GetPoints() )

		enclosedPointSelector = vtkSelectEnclosedPoints()
		enclosedPointSelector.CheckSurfaceOn()
		enclosedPointSelector.SetInputData( pts )
		enclosedPointSelector.SetSurfaceData( surfaceMesh )
		enclosedPointSelector.SetTolerance( 1e-9 )
		enclosedPointSelector.Update()
		enclosedPoints = enclosedPointSelector.GetOutput()

		for i in range(0, targetGrid.GetNumberOfPoints() ):
			if enclosedPointSelector.IsInside(i):
				df.SetTuple1( i, -df.GetTuple1(i) )     # invert sign

	targetGrid.GetPointData().AddArray( df )

# VOXELIZE_DISPLACEMENT
def interpolateToGrid( mesh, grid, cellSize, sharpness=10, radius=None ):
	"""     
	Interpolates DataArrays present in mesh onto the grid using a Gaussian Kernel. 
	Be careful because all DataArrays belonging to mesh will be interpolated.
	Default kernel radius is defined as 5*cellSize.

	Arguments:
	---------
	mesh (vtkDataSet):
		Initial mesh with the fields to be interpolated into grid.
	grid (vtkDataSet):
		Grid where the DataArrays will be interpolated to.
	cellSize (list of ints):
		Size of the grid cells.
	sharpness (int):
		Sharpness of the Gaussian kernel. As the sharpness increases, the effect of distant points decreases.
		Default: 10
	radius (float):
		Radius of the Gaussian kernel. If not specified, kernel radius is defined as 5*cellSize.
		Default: None

	Returns:
	--------
	vtkDataSet
		Same as mesh_initial where a new DataArray "displacement" has been added
	"""
	if not radius:
		radius = cellSize * 5

	# Perform the interpolation
	interpolator = vtkPointInterpolator()
	gaussianKernel = vtkGaussianKernel()
	gaussianKernel.SetRadius( radius )
	gaussianKernel.SetSharpness(sharpness)
	interpolator.SetSourceData( mesh )
	interpolator.SetInputData( grid )
	interpolator.SetKernel( gaussianKernel )
	#interpolator.SetNullPointsStrategy(vtkPointInterpolator.CLOSEST_POINT)
	interpolator.Update()

	#output = interpolator.GetOutput()
	output = vtkStructuredGrid()
	output.DeepCopy(interpolator.GetOutput())
	del interpolator, gaussianKernel
	return output

def spreadValuesOnGridPoints( mesh, grid, field_name, radius=None, binary=False ):
	""" 
	Transfers the mesh field (Tuple1) described by field_name to the grid.
	For each mesh point, finds the grid cell it falls into and associated the mesh field value
	to all the grid nodes defining the corresponding cell. If radius is specified, it associates the 
	the mesh field value to all the grid nodes whose distance from the corresponding mesh point 
	is lower than that radius. 
	Note that no interpolation is computed here (values are just transferred to grid as they are).

	Arguments:
	---------
	mesh (vtkDataSet):
		Topology of the mesh
	grid (vtkDataSet):
		Topology of the grid
	field_name (str):
		Name of the DataArray present in mesh that has to be transferred to the grid.
	radius (float):
		Maximum distance between a cell point and a mesh point to have the mesh field associated.
		Default: None 
	binary (bool):
		If True, when field is different from 0, sets the grid value to 1.
		Default: False

	Returns:
	--------
	vtkDataSet
		Same as grid where the new DataArray "field_name" has been added
	"""

	# Create array for grid
	grid_array = vtkDoubleArray()
	grid_array.SetNumberOfComponents(1)
	grid_array.SetName(field_name)
	grid_array.SetNumberOfTuples(grid.GetNumberOfPoints())
	grid_array.Fill(0.0)

	if radius:
		locator = vtkPointLocator()
	else:
		locator = vtkCellLocator()
	locator.SetDataSet(grid)
	locator.Update()
	cell_pts_idx = vtkIdList()

	# Get data array in mesh
	mesh_array = mesh.GetPointData().GetArray(field_name)

	for i in range(mesh_array.GetNumberOfTuples()):
		field_value = mesh_array.GetTuple1(i)
		if not field_value == 0.0:

			if radius:
				locator.FindPointsWithinRadius( radius, mesh.GetPoint(i), cell_pts_idx)
			else:
				cell = locator.FindCell(mesh.GetPoint(i))
				grid.GetCellPoints(cell,cell_pts_idx)

			if binary:
				field_value = 1.0
			for j in range(cell_pts_idx.GetNumberOfIds()):
				grid_array.SetTuple1(cell_pts_idx.GetId(j), field_value)

	grid.GetPointData().AddArray( grid_array )   
	return grid

def get_distance_np( array1, array2 ):
	"""
	For each point in array2, returns the Euclidean distance with its closest point in array1 and
	the index of such closest point. 
	
	Arguments
	----------
	array1 (ndarray):
		N1 x M array with the points.
	array2 (ndarray):
		N2 x M array with the points.
	
	Returns
	----------
	dist:
		N2 x 1 Euclidean distance of each point of array2 with the closest point in array1.
		dist contains the same result as: 	np.nanmin( distance.cdist( array1, array2 ), axis=1 )
	indexes:
		N2 x 1 Indices of closest point in array1

	"""
	mytree = spatial.cKDTree(array1)
	dist, indexes = mytree.query(array2)
	return dist, indexes

def compute_spherical_roi( center_position, radius, volume_points ):
	"""
	Returns the indices of points in volume_points which fall closer than radius from center_position.
	
	Arguments
	----------
	center_position (ndarray):
		1 x 3 array (or list) with coordinates of the ROI center.
	radius (float):
		Max distance from the center to include a point in the ROI.
	volume_points (ndarray):
		N x 3 array with the points.
	
	Returns
	----------
	indices:
		list of indices of volume_points falling within the ROI.

	"""
	if isinstance(center_position, list):
		center_position = np.asarray(center_position)
	center_position = center_position.reshape((1,-1))
	dist, idx = get_distance_np( center_position, volume_points )
	indices = np.where(dist <= radius)[0]
	return indices.tolist()