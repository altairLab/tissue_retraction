#!/usr/bin/env python
import os
import numpy as np
from scipy import spatial

# ROS IMPORT
import rospy, rospkg
from std_msgs.msg import Header, Int32
from sensor_msgs.msg import PointCloud2, PointField
from geometry_msgs.msg import PoseStamped
from sensor_msgs import point_cloud2
import message_filters
from dvrk_task_msgs.msg import ActionArray

fields = [
	PointField('x', 0, PointField.FLOAT32, 1),
	PointField('y', 4, PointField.FLOAT32, 1),
	PointField('z', 8, PointField.FLOAT32, 1),
	]

#################################################
class Monitor(object):
	def __init__(self):

		self.num_baseline 	= 0
		self.baseline_sum	= 0.0
		self.baseline_error = 0.0

		self.first_grasp   = False
		self.has_grasped   = False
		
		self.uncertain_thr 		= [0.002, 0.004, 0.006] # LOW-MEDIUM-HIGH thresholds in m (RELATIVE to the error at rest)
		self.is_uncertain_code  = [0, 1, 2, 3]		 # uncertainty code (0 means "not uncertain"=real state follows simulation)
		self.is_uncertain 	    = 0
	
		# Publisher of uncertainty metric
		self.is_uncertain_pub = rospy.Publisher(rospy.get_param("uncertainty_topic"), Int32, queue_size=1)
		self.is_uncertain_pub.publish(Int32(self.is_uncertain))

		# Subscribers to real point clouds and dvrk topics
		sim_tissue_sub  = message_filters.Subscriber(rospy.get_param("sofa_tissue_topic"), PointCloud2)
		real_tissue_sub = message_filters.Subscriber(rospy.get_param("pcl_tissue_topic"), PointCloud2) 
		psm1_sub		= message_filters.Subscriber(rospy.get_param("psm1_topic"), PoseStamped)
		psm2_sub		= message_filters.Subscriber(rospy.get_param("psm2_topic"), PoseStamped)
		action_sub	    = rospy.Subscriber(rospy.get_param("action_topic"), ActionArray, self.action_callback)

		ts1 = message_filters.ApproximateTimeSynchronizer([sim_tissue_sub, real_tissue_sub], 20, 5, allow_headerless=False)
		ts1.registerCallback(self.compare_online)

	def action_callback(self, msg):
		if msg.action_list[0].action == "grasp": 
			self.first_grasp = True
	
	def compare_online(self, sim_pcd, real_pcd):
		# Reading the received pcds
		sim_points  = np.asarray(point_cloud2.read_points_list(sim_pcd, field_names=('x', 'y', 'z'), skip_nans=False))
		real_points = np.asarray(point_cloud2.read_points_list(real_pcd, field_names=('x', 'y', 'z'), skip_nans=False))

		d, i = get_closest_points(real_points, sim_points)
		d_max = np.median(d)

		# print 'Abs error ', d_max

		if self.first_grasp:
			rospy.loginfo('Average baseline error: '+str(self.baseline_error))
			self.first_grasp  = False
			self.has_grasped  = True

		if self.has_grasped:
			d_rel = np.abs(d_max - self.baseline_error)
			if d_rel <= self.uncertain_thr[0]:
				self.is_uncertain = self.is_uncertain_code[0]
				#rospy.loginfo('BELOW THRESH '+str(self.is_uncertain_code[1]))
			elif d_rel >= self.uncertain_thr[-1]:
				self.is_uncertain = self.is_uncertain_code[-1]
				rospy.logwarn('OVER THRESH '+str(self.is_uncertain_code[-1]))
			else:
				for k in range(len(self.uncertain_thr)-1):
					if d_rel > self.uncertain_thr[k] and d_rel <= self.uncertain_thr[k+1]:
						rospy.logwarn('OVER THRESH '+str(self.is_uncertain_code[k+1]))
						self.is_uncertain = self.is_uncertain_code[k+1]
						break

			pub_msg = Int32(self.is_uncertain)
			self.is_uncertain_pub.publish(pub_msg)

		else:
			self.num_baseline += 1
			self.baseline_sum += d_max
			self.baseline_error = self.baseline_sum/self.num_baseline

def get_closest_points( array1, array2 ):
	"""
	For each point in array2, returns the Euclidean distance with its closest point in array1 and the index of such closest point. 
	"""
	mytree = spatial.cKDTree(array1)
	dist, indexes = mytree.query(array2)
	return dist, indexes

def publish_pointcloud2(points, topic):
	header = Header()
	header.frame_id = rospy.get_param("fixed_frame")
	header.stamp = rospy.Time.now()
	pcl_msg = point_cloud2.create_cloud(header, fields, points.tolist())
	topic.publish(pcl_msg)

def main():

	rospy.init_node('monitoring_node')
	m = Monitor()

	r = rospy.Rate(1)
	rospy.sleep(1.)

	while not rospy.is_shutdown():
		r.sleep()

if __name__ == '__main__':
	if rospy.get_param("study") == "":
		main()


