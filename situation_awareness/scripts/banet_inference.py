#!/usr/bin/env python
import os, sys
import time
import copy
import numpy as np

from vtk import *
from vtk.util import numpy_support

# MATCHING IDS IMPORT
from plyfile import PlyData
import scipy.sparse.linalg as sla
import robust_laplacian
from pyflann import *

# INFERENCE IMPORT
abs_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(abs_path, '../banet'))
import torch
from banet import BANet
import utils

# ROS IMPORT
import rospy
from sensor_msgs.msg import PointCloud2, PointField, JointState
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Float32MultiArray, Int32MultiArray
from sensor_msgs import point_cloud2
import message_filters
from dvrk_task_msgs.msg import ActionArray
import struct

flann = FLANN()

# To convert from world to sofa (= BANet reference) and back
tf_world2sofa = np.asarray( rospy.get_param("world2sofa") ).reshape((4,4) )
tf_sofa2world = np.linalg.inv(tf_world2sofa)

fields = [
	PointField('x', 0, PointField.FLOAT32, 1),
	PointField('y', 4, PointField.FLOAT32, 1),
	PointField('z', 8, PointField.FLOAT32, 1),
	PointField('rgba', 12, PointField.UINT32, 1),
	]
rgba_red    = struct.unpack('I', struct.pack('BBBB', 0, 0, 255, 255))[0]
rgba_green  = struct.unpack('I', struct.pack('BBBB', 0, 255, 0, 255))[0]

#################################################
class BANetSA(object):
	def __init__(self):

		self.debug = False
		self.start_inference = False

		# banet
		self.device = torch.device("cpu" if not torch.cuda.is_available() else "cuda:0")
		self.banet = BANet(in_channels=4, out_channels=1, init_features=8)
		weights = os.path.join(abs_path, '../banet/weights.pt')
		try:
			state_dict = torch.load(weights, map_location=self.device)
		except Exception as e:
			print(e)
		
		self.banet.load_state_dict(state_dict)
		self.banet.eval()
		self.banet.to(self.device)
		print('Loaded banet')

		template_filename = os.path.join(abs_path,'../../simulation/data/template_lr.ply')

		# zoomout
		self.zoomout = True
		print(template_filename)
		self.N_eigs_init  = 10
		self.N_eigs_final = 100
		self.step = 1
		self.evecs_T = get_mesh_evecs(template_filename, self.N_eigs_final)

		template = PlyData.read(template_filename)
		self.template_vertices = np.vstack(( template['vertex']['x'], template['vertex']['y'], template['vertex']['z'])).T.astype(np.float32)
		template_vtk = utils.numpy2polydata(self.template_vertices)

		# additional processing
		self.decimate = False
		self.num_points = 500

		# voxelization
		surface_filename = os.path.join(abs_path,'../../simulation/data/surface_lr.stl')
		volume_filename  = os.path.join(abs_path,'../../simulation/data/volume_lr.vtk')
		self.min_visible_displacement = 0.01 # Minimum displacement which is associated to non-zero displacement on the grid
		self.grid_side_length = 0.3       # Size of voxelized grid along every dimension, in meters
		self.grid_size = 64   # Number of cells of voxelized grid along every dimension
		
		self.surface_mesh = utils.loadMesh( surface_filename )
		self.volume_mesh  = utils.loadMesh( volume_filename )

		# Initial steps
		self.tf, self.Tx, self.Ty, self.Tz = utils.get_centering_transform(self.surface_mesh) # Centering (needed for sdf)
		self.template_vtk = utils.apply_vtk_transform( template_vtk, self.tf )
		self.surface_mesh = utils.apply_vtk_transform( self.surface_mesh, self.tf )
		self.volume_mesh  = utils.apply_vtk_transform( self.volume_mesh, self.tf )

		self.volume_mesh  = utils.unstructuredGridToPolyData( self.volume_mesh )
		self.volume2templateIds = utils.getClosestPoints(self.template_vtk, self.volume_mesh)
		
		self.has_ground_truth = False 
		if self.volume_mesh.GetPointData().HasArray("stiffness"): # If ground truth "stiffness" array is present in volume mesh
			self.has_ground_truth = True

		# Compute sdf
		print('Creating grid...')
		self.grid = utils.createGrid(self.grid_side_length, self.grid_size) # Create grid
		utils.storeTransformationMatrix( self.grid, self.tf )
		utils.distanceField( self.surface_mesh, self.grid, "preoperativeSurface", signed=True ) # sdf for surface

		# Convert sdf to numpy
		preoperativeRaw = numpy_support.vtk_to_numpy( self.grid.GetPointData().GetArray("preoperativeSurface") )
		preoperative_sdf = np.reshape( preoperativeRaw, (self.grid_size, self.grid_size, self.grid_size, 1) )
		preoperative_sdf = np.transpose(preoperative_sdf, (3,0,1,2) )
		max_mask_value = (self.grid_side_length/self.grid_size)*np.sqrt(3)
		self.mask = (preoperative_sdf <= max_mask_value)
		self.preoperative_sdf = preoperative_sdf*self.mask

		# Keep track of previous predictions
		self.num_preds_to_keep = 30
		self.mask_id 	   = 0
		self.curr_num_pred = 1
		self.num_points  = self.volume_mesh.GetNumberOfPoints()
		self.pred_masks	 = np.zeros((self.num_preds_to_keep,self.num_points))
		self.min_perc_occurrences = 0.8 # keep indices which have been predicted as fixed at least 80% of the times

		# Max number of predicted points
		self.grasp_tissue_ids = []
		self.max_num_pred_points = int( 0.6*self.surface_mesh.GetNumberOfPoints() / 2 ) # 40% of all the surface points 
		
		# Volume points in world coordinates
		volume_mesh_to_transform = vtkUnstructuredGrid()
		volume_mesh_to_transform.DeepCopy(self.volume_mesh)
		self.volume_points = numpy_support.vtk_to_numpy( volume_mesh_to_transform.GetPoints().GetData())
		for p in range(self.volume_points.shape[0]):
			self.volume_points[p] = self.point2world(self.volume_points[p])

		# Init fixed points from simulation
		sofa_fixed_pcd    = rospy.wait_for_message(rospy.get_param("pcl_fixed_topic"), PointCloud2)
		sofa_fixed_points = np.asarray(point_cloud2.read_points_list(sofa_fixed_pcd, field_names=('x', 'y', 'z'), skip_nans=False))
		__, self.sofa_fixed_init =  utils.get_distance_np(self.volume_points, sofa_fixed_points)
		
		# Get roi position from simulation
		target_msg 		= rospy.wait_for_message(rospy.get_param("roi_position_topic"), Float32MultiArray)
		target_position = np.asarray(target_msg.data)
		self.target_closest_ids = utils.compute_spherical_roi(target_position, 0.015, self.volume_points)

		print('Waiting for the PSM to grasp and pull the tissue...')

		# Publishers
		self.banet_pcd_pub = rospy.Publisher(rospy.get_param("banet_fixed_topic"), PointCloud2, queue_size=1)
		self.banet_ind_pub = rospy.Publisher(rospy.get_param("banet_indices_topic"), Int32MultiArray, queue_size=1)

		if self.debug:
			self.banet_ground_pub = rospy.Publisher('/banet_template', PointCloud2, queue_size=1)
		
		# Subscriber to pcd
		pcd_listener 	= rospy.Subscriber(rospy.get_param("pcl_tissue_topic"), PointCloud2, self.inference_cb, queue_size=1)
		
		# Subscribers to psm topics
		self.grasp_psm   = None
		self.grasp_pose  = None
		self.min_psm_displacement = 0.008
		psm1_sub		= message_filters.Subscriber(rospy.get_param("psm1_topic"), PoseStamped)
		psm2_sub		= message_filters.Subscriber(rospy.get_param("psm2_topic"), PoseStamped)
		ts = message_filters.ApproximateTimeSynchronizer([psm1_sub, psm2_sub], 10, 0.2, allow_headerless=False)
		ts.registerCallback(self.inference_trigger_callback)

		action_sub	    = rospy.Subscriber(rospy.get_param("action_topic"), ActionArray, self.action_callback)

	def inference_cb(self, data):

		# Start inference only if psm has grasped and pulled the tissue a bit
		if self.start_inference:

			if self.debug:
				start = time.time()

			# Reading the pcl from ros message
			points = np.asarray(point_cloud2.read_points_list(data, field_names=('x', 'y', 'z'), skip_nans=False))

			if self.decimate:
				if points.shape[0] == 0:
					return
				
				# Decimating the mesh
				decimate_indices = np.random.choice(np.arange(points.shape[0]), size=self.num_points)
				dec_points = points[decimate_indices]
				points     = dec_points

			# Transform points in required reference system (i.e., sofa)
			points_transf = []
			for row in points:
				x,y,z    = self.point2sofa(row)
				points_transf.append([x, y, z])
			points_transf = np.asarray(points_transf)
			
			if self.volume_points.shape == points_transf.shape:
				# Bypass zoomout for better accuracy and save computation time for experiments in simulation, where corresponding points are implicitly defined.
				self.zoomout = False
				self.template_vtk = self.volume_mesh
				points_transf = points_transf[self.volume2templateIds, :] 
				closestIndicesTemplate = self.volume2templateIds		
			
			# Saving the pcd into vtkPolyData
			intraopSurface = utils.numpy2polydata(points_transf)

			# Remove previously computed displacement
			if self.grid.GetPointData().HasArray("displacement"):
				self.grid.GetPointData().RemoveArray("displacement")
			if self.template_vtk.GetPointData().HasArray("displacement"):
				self.template_vtk.GetPointData().RemoveArray("displacement")
			if self.volume_mesh.GetPointData().HasArray("displacement"):
				self.volume_mesh.GetPointData().RemoveArray("displacement")

			# Compute corresponding points with zoomout
			if self.zoomout:
				# Laplacian on pcl
				L, M = robust_laplacian.point_cloud_laplacian(points_transf)
				evals, evecs_pc = sla.eigsh(L, self.N_eigs_final, M, sigma=1e-8)
				
				# kNN to get first indices
				result_ini, dists = flann.nn(self.template_vertices, points_transf.astype(np.float32))
			
				# Applying ZOOM-OUT
				C_iter = np.matmul(np.linalg.pinv(evecs_pc[:,0:self.N_eigs_init]),self.evecs_T[result_ini,0:self.N_eigs_init])
				for i in np.arange(0,self.N_eigs_final - self.N_eigs_init, self.step):
				# 1) Convert into dense correspondence
					evecs_transf = np.matmul(self.evecs_T[:,0:self.N_eigs_init+i], C_iter.T)
					result, dists = flann.nn(evecs_transf, evecs_pc[:,0:self.N_eigs_init+i], 1)
				# 2) Convert into C of dimension (n+1) x (n+1)
					C_iter = np.matmul(np.linalg.pinv(evecs_pc[:,0:self.N_eigs_init+i+1]),self.evecs_T[result,0:self.N_eigs_init+i+1])
				
				closestIndicesTemplate = result

			# Calculate input displacement 
			displacement = utils.compute_warp_displacement( self.template_vtk, intraopSurface, closestIndicesTemplate, min_visible_displacement=self.min_visible_displacement )

			self.template_vtk.GetPointData().AddArray( displacement )
			
			cellSize = self.grid_side_length/(self.grid_size)
			output_grid = utils.interpolateToGrid( self.template_vtk, self.grid, cellSize )

			# Displacement to numpy
			grid_data = output_grid.GetPointData()
			displacementRaw = numpy_support.vtk_to_numpy( grid_data.GetArray("displacement") )
			displacement = np.reshape( displacementRaw, (self.grid_size, self.grid_size, self.grid_size, 3) )
			displacement = np.transpose( displacement, (3,0,1,2) )
			displacement = displacement*self.mask

			x_sample = np.append(self.preoperative_sdf, displacement, axis=0)
			
			# Numpy to torch
			input_tensor = torch.from_numpy(x_sample.astype(np.float32))
			mask_tensor  = torch.from_numpy(self.mask.astype(np.bool))
			
			# Adding a dimension to match torch requirements
			input_tensor = input_tensor.unsqueeze(0)
			
			with torch.set_grad_enabled(False):
				input_tensor, mask_tensor = input_tensor.to(self.device), mask_tensor.to(self.device)

				y = self.banet(input_tensor)
				y_pred = torch.sigmoid(y)*mask_tensor 
		
			prediction = y_pred.detach().cpu().numpy()
			prediction = np.round(prediction).astype(int)

			# Remove first dimension and flatten
			prediction = prediction[0, :, :, :, :]
			prediction = prediction.flatten()

			locator = vtkCellLocator()
			locator.SetDataSet(output_grid)
			locator.Update()
			cell_pts_idx = vtkIdList()

			pred_points = []
			pred_indices = []

			for idx in range(self.volume_mesh.GetNumberOfPoints()):
				volume_point = self.volume_mesh.GetPoint(idx)

				cell = locator.FindCell(volume_point)
				output_grid.GetCellPoints(cell, cell_pts_idx)

				for j in range(cell_pts_idx.GetNumberOfIds()):
					if prediction[cell_pts_idx.GetId(j)] == 0:
						break

				# if the point is within a cell with 1 on all the boundary points
				if j >= cell_pts_idx.GetNumberOfIds()-1:
					pred_indices.append(idx)
					
			if self.debug:
				template_dvrk = []
				for x,y,z in self.template_vertices:
					x_, y_, z_ = self.point2world([x,y,z])
					template_dvrk.append([x_, y_, z_, rgba_green])

				pcl_gr = point_cloud2.create_cloud(data.header, fields, template_dvrk)
				self.banet_ground_pub.publish(pcl_gr)

			# If prediction is not empty
			if len(pred_indices) > 0:
				pred_ind_mask = [1 if i in pred_indices else 0 for i in range(self.num_points)]
				pred_ind_mask = np.asarray(pred_ind_mask)

				self.pred_masks[self.mask_id,:] = pred_ind_mask
				self.mask_id += 1

				# Reset mask id to keep track only of the last self.num_preds_to_keep predictions
				if self.mask_id >= self.num_preds_to_keep:
					self.mask_id = 0
								
				# Fix indices which have been predicted more than (min_perc_occurrences x curr_num_pred) times
				pred_ind_count = np.sum(self.pred_masks, axis=0)
				pred_indices = [j for j in range(self.num_points) if pred_ind_count[j]>=self.curr_num_pred*self.min_perc_occurrences]

				# If we are predicting too many points as fixed, keep
				if len(pred_indices) > self.max_num_pred_points:
					rospy.logwarn("Predicting too many fixed points! Probably grasping a fixed region")
					diff = [d for d in self.sofa_fixed_init if not(d in self.grasp_tissue_ids)]
					pred_indices = copy.deepcopy(self.grasp_tissue_ids)
					pred_indices.extend(diff)

				# If we are predicting points too close to the target
				target_closest_pred = [t for t in self.target_closest_ids if t in pred_indices]
				if len(target_closest_pred):
					rospy.logwarn("Removing predicted points too close to the target")
					for t in target_closest_pred:
						pred_indices.remove(t)

				# Increment counter of pred number only if num_preds_to_keep has not been reached
				if self.curr_num_pred < self.num_preds_to_keep:
					self.curr_num_pred += 1

				# Extract fixed points
				selected_points = self.volume_points[pred_indices,:]
				selected_points = selected_points.tolist()
				pred_points = []
				for s in selected_points:
					s.append(rgba_red)
					pred_points.append(s)
				
				# Publish predictions
				print("Number of banet predicted points: ",len(pred_points))
				pcl_ba = point_cloud2.create_cloud(data.header, fields, pred_points)
				self.banet_pcd_pub.publish(pcl_ba)
				self.banet_ind_pub.publish( Int32MultiArray(data=pred_indices) )
				#rospy.loginfo(pred_indices)
	
			else:
				rospy.logwarn('Zero predicted points, discarding prediction...')

			if self.debug:
				end = time.time()
				print("Time to publish: ", str(end - start))

	def inference_trigger_callback(self, psm1_pose, psm2_pose):
		# If psm has grasped but inference has not started yet
		if (self.grasp_psm is not None) and not(self.start_inference):		
			# Check if psm has travelled enough distance to start inference
			if self.grasp_psm == 'psm1':
				curr_psm_pose = np.asarray([psm1_pose.pose.position.x, psm1_pose.pose.position.y, psm1_pose.pose.position.z])
			else:
				curr_psm_pose = np.asarray([psm2_pose.pose.position.x, psm2_pose.pose.position.y, psm2_pose.pose.position.z])
			
			# If psm has just grasped
			if self.grasp_pose is None:
				self.grasp_pose = curr_psm_pose
					
			motion = np.linalg.norm(curr_psm_pose-self.grasp_pose)
			if motion >= self.min_psm_displacement:
				print('PSM has moved more than ', self.min_psm_displacement, 'from grasp, starting inference...')
				self.start_inference = True

	def action_callback(self, msg):
		if msg.action_list[0].action == "grasp": 
			self.grasp_psm = msg.action_list[0].robot

			grasp_msg = rospy.wait_for_message(rospy.get_param("sofa_grasped_topic"), Int32MultiArray)
			self.grasp_tissue_ids = list(grasp_msg.data)
			
		elif msg.action_list[0].action == "release":
			self.reset()

	def reset(self):
		rospy.loginfo('Changing grasping point: stopping inference')
		self.start_inference = False
		self.grasp_psm  = None
		self.grasp_pose = None
			
	def point2world(self, point):
		point    = np.append(point,1)
		x,y,z,__ = tf_sofa2world.dot(point)
		x,y,z    = np.asarray( [x-self.Tx, y-self.Ty, z-self.Tz] )
		return x,y,z

	def point2sofa(self, point):
		point    = np.append(point,1)
		x,y,z,__ = tf_world2sofa.dot(point)
		x,y,z    = np.asarray( [x+self.Tx, y+self.Ty, z+self.Tz] )
		return x,y,z

def get_mesh_evecs(mesh_filename, n_eig):
	mesh = PlyData.read(mesh_filename)
	template_vertices = np.vstack((
		mesh['vertex']['x'],
		mesh['vertex']['y'],
		mesh['vertex']['z']
	)).T.astype(np.float32)
	template_faces = np.vstack(mesh['face']['vertex_indices'])

	L_T, M_T = robust_laplacian.mesh_laplacian(template_vertices, template_faces)
	__, evecs_T = sla.eigsh(L_T, n_eig, M_T, sigma=1e-8)
	return evecs_T


def main():

	rospy.init_node('banet_node')
	bn = BANetSA()
	rospy.sleep(1.)

	while not rospy.is_shutdown():
		rospy.sleep(.1)

if __name__ == '__main__':
	main()


