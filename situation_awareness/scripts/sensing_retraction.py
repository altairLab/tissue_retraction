#!/usr/bin/env python2
import numpy as np
import math
import os

import rospy
import sys
from std_msgs.msg import Header, Int32, Bool, Float32, Float32MultiArray as Flt_arr
from sensor_msgs.msg import PointCloud2
from sensor_msgs import point_cloud2 as pc2
from geometry_msgs.msg import PoseArray, PoseStamped, Pose, Point
from tf import TransformListener
from tf_conversions import posemath as pm
from tf.transformations import quaternion_multiply as rotate
sys.path.append('~/catkin_ws/src/tissue_retraction/robot_control/scripts')
from dvrk_task_msgs.msg import ContextModel, ActionArray, PoseStampedArray, PC2Array
import dvrk
# from pcl import VoxelGridFilter as make_grid

#comm class
class Situation_awareness(object):
    def __init__(self):
        self.dvrk_frame_id  = rospy.get_param("dvrk_frame_id")
        self.grid_size = rospy.get_param("grid_size")
        self.test = rospy.get_param("test") #offline plan comparison if True

        self.dim_grid_x = self.grid_size #coordinates in simulation
        self.dim_grid_y = self.grid_size #coordinates in simulation
        self.psm1 = dvrk.psm('PSM1')
        self.psm2 = dvrk.psm('PSM2')
        self.psm1_standard_orient = None
        self.psm2_standard_orient = None
        self.old_fluents = []
        self.state = [] #FSM state
        self.manip_id = [] #the name of the manipulator to be used to perform the prescribed action
        self.block = np.empty(self.dim_grid_x*self.dim_grid_y, np.object) #ID of the block as a matrix of 3D centroids
        self.block_id = [] #grid coordinates of blocks for ASP
        self.fixed = [] #ID of the block as a matrix of Bool
        self.failure = False
        self.warning = False
        self.location = []
        self.request = 0 # request ID --- 1 for fluents, 2 for failure detection
        self.tf_list = TransformListener()
        self.tissue_center = PoseStamped()
        self.roi_pose = PoseStamped()
        self.received_fixed = False
        self.received_tissue = False
        self.visible = 0. #percentage 0-100 of visibility of tumor
        #TO COMPUTE BLOCKS FROM PC2
        self.tissue_pts = []
        self.fixed_pts = []
        self.forces = []
        self.stress = []
        self.z_mean = 0.
        self.max_force = 0.
        self.force_thr = 0.3
        self.visible_thr = 80. #percentage
        self.init_warning_time = rospy.Time.now()
        self.warning_time = rospy.Duration(2.)
        #TO STOP PULLING AFTER FORCE FEEDBACK
        self.max_height_1 = False
        self.max_height_2 = False
        #TO STOP PULLING AFTER POSITION FEEDBACK
        self.max_height = 0.04
        self.discarded = []
        self.unstable = False
        self.unsat = False
        self.force_fail_times = 0
        self.monitoring = 0
        self.unstable_listener = rospy.Subscriber("unstable_test", Bool, self.unstable_cb)
        self.unsat_listener = rospy.Subscriber("unsat_test", Bool, self.unsat_cb)

        self.fluent_pub = rospy.Publisher(rospy.get_param("context_topic"), ContextModel, queue_size=1)
        self.target_pose_pub = rospy.Publisher(rospy.get_param("target_topic"), PoseStampedArray, queue_size=1)
        self.failure_pub = rospy.Publisher(rospy.get_param("failure_topic"), Bool, queue_size=1)
        self.warn_pub = rospy.Publisher(rospy.get_param("warning_topic"), Bool, queue_size=1)
        self.fluent_comp_status_pub = rospy.Publisher(rospy.get_param("fluent_status_topic"), Bool, queue_size=1)
        self.tissue_center_pub = rospy.Publisher(rospy.get_param("tissue_center_topic"), PoseStamped, queue_size=1)
        self.test_pub = rospy.Publisher("/testing_blocks", PoseArray, queue_size=1)
        self.test_target_pub = rospy.Publisher("/testing_target", PoseStamped, queue_size=1)

        self.actions = []
        self.init_time = rospy.Time.now() #for plan execution time
        self.cloud_time = rospy.Time.now() #for cloud acquisition
        self.distance = 0.
        self.max_forces = []
        self.stress_list = []
        self.monitoring_arr = []
        self.cloud_array = PC2Array()
        self.psm1_poses = PoseArray()
        self.psm2_poses = PoseArray()
        self.psm1_pose_start = list(self.psm1.get_current_position().p)
        self.psm2_pose_start = list(self.psm2.get_current_position().p)
        self.end_pub = rospy.Publisher("/ended_test", Bool, queue_size=1)
        self.force_pub = rospy.Publisher("/force_test", Flt_arr, queue_size=1)
        self.stress_pub = rospy.Publisher("/stress_test", Flt_arr, queue_size=1)
        self.time_pub = rospy.Publisher("/time_test", Float32, queue_size=1)
        self.distance_pub = rospy.Publisher("/distance_test", Float32, queue_size=1)
        self.visibility_pub = rospy.Publisher("/visibility_test", Float32, queue_size=1)
        self.actions_pub = rospy.Publisher("/actions_test", ActionArray, queue_size=1)
        self.cloud_pub = rospy.Publisher("cloud_test", PC2Array, queue_size=1)
        self.psm1_pub = rospy.Publisher("psm1_test", PoseArray, queue_size=1)
        self.psm2_pub = rospy.Publisher("psm2_test", PoseArray, queue_size=1)
        self.monitoring_pub = rospy.Publisher("monitoring_test", Flt_arr, queue_size=1)
        self.force_fail_pub = rospy.Publisher("force_fail_test", Int32, queue_size=1)

        if self.test:
            self.tissue_listener = rospy.Subscriber(rospy.get_param("sofa_tissue_topic"), PointCloud2, self.tissue_cb)
            self.fixed_listener = rospy.Subscriber(rospy.get_param("pcl_fixed_topic"), PointCloud2, self.fixed_cb)
        else:
            self.tissue_listener = rospy.Subscriber(rospy.get_param("pcl_tissue_topic"), PointCloud2, self.tissue_cb)
            self.fixed_listener = rospy.Subscriber(rospy.get_param("pcl_fixed_topic"), PointCloud2, self.fixed_cb) #should be BaNet in real scenario

        self.state_listener = rospy.Subscriber(rospy.get_param("action_topic"), ActionArray, self.state_callback)
        self.sensing_listener = rospy.Subscriber(rospy.get_param("sensing_request_topic"), Int32, self.on_sensing_request)
        self.visible_listener = rospy.Subscriber(rospy.get_param("roi_visibility_topic"), Float32, self.visible_callback)
        self.roi_pos_listener = rospy.Subscriber(rospy.get_param("roi_position_topic"), Flt_arr, self.roi_pos_callback)
        self.motion_status_listener = rospy.Subscriber(rospy.get_param("motion_status_topic"), Bool, self.motion_status_cb)
        self.force_listener = rospy.Subscriber(rospy.get_param("force_topic"), Flt_arr, self.force_callback)
        self.stress_listener = rospy.Subscriber(rospy.get_param("stress_topic"), Float32, self.stress_callback)
        self.monitoring_listener = rospy.Subscriber(rospy.get_param("uncertainty_topic"), Int32, self.monitoring_cb)

        while not self.received_tissue or not self.received_fixed:
            pass
        #GENERATE ATOMS FOR BLOCKS
        while self.block.all() == None or (True not in self.fixed and rospy.Time.now() - self.init_time < rospy.Duration(5.)):
            if rospy.is_shutdown():
                break
            try:
                self.gen_blocks()
            except Exception as e:
                rospy.logwarn("RECEIVING EMPTY POINT CLOUDS?")
                rospy.logwarn(str(e))


    def on_sensing_request(self, data):
        self.request = data.data

    def unstable_cb(self, data):
        self.unstable = True

    def unsat_cb(self, data):
        self.unsat = True
    
    def monitoring_cb(self, data):
        self.monitoring = data.data
    
    def visible_callback(self, data):
        self.visible = data.data

    def roi_pos_callback(self, data):
        self.roi_pose = PoseStamped(header=Header(stamp = rospy.Time(0.), frame_id=rospy.get_param("fixed_frame")), 
                                    pose = Pose(position=Point(x=data.data[0], y=data.data[1], z=data.data[2])))

    def force_callback(self, data):
        forces = []
        for i in range(int(len(data.data)/3)):
            forces.append(np.linalg.norm([data.data[3*i], data.data[3*i+1], data.data[3*i+2]]))
        
        #EXCLUDE OUTLIERS
        if len(self.forces) < 10: 
            self.forces.append(max(forces))
        else:
            self.max_force = np.median(self.forces)
            self.max_forces.append(self.max_force)
            self.monitoring_arr.append(self.monitoring)
            self.forces = self.forces[1:]  

    def stress_callback(self, data):
        
        #EXCLUDE OUTLIERS
        if len(self.stress) < 10: 
            self.stress.append(data.data)
        else:
            self.max_stress = np.median(self.stress)
            self.stress_list.append(self.max_stress)
            self.stress = self.stress[1:] 

    def motion_status_cb(self, data):
        self.reset()
        #FOR TEST
        self.distance += np.linalg.norm(np.array(list(self.psm1.get_current_position().p)) - np.array(self.psm1_pose_start))
        self.distance += np.linalg.norm(np.array(list(self.psm2.get_current_position().p)) - np.array(self.psm2_pose_start))
        self.psm1_pose_start = list(self.psm1.get_current_position().p)
        self.psm2_pose_start = list(self.psm2.get_current_position().p)
        #FOR TEST

        for i in range(len(self.state)):
            if self.state[i] in ["pull", "move_away", "move_ROI"]:       
                if self.manip_id[i] == "psm1":
                    psm = self.psm1
                else:
                    psm = self.psm2
                psm_base = PoseStamped(header = Header(stamp = rospy.Time(0.), frame_id = psm.name().upper() + "_base"),
                                            pose = pm.toMsg(psm.get_current_position()))
                self.tf_list.waitForTransform(rospy.get_param("fixed_frame"), psm.name().upper() + "_base"+self.dvrk_frame_id, time=psm_base.header.stamp, timeout=rospy.Duration(10.))
                psm_world = self.tf_list.transformPose(rospy.get_param("fixed_frame"), psm_base)

                block_id_str = self.block_id[i].replace("(", "").replace(")", "").replace(" ", "")
                comma = block_id_str.find(",")
                block_id = (int(block_id_str[0:comma])-1, int(block_id_str[(comma+1):])-1)
                self.block[block_id[0]][block_id[1]][0] = psm_world.pose.position.x
                self.block[block_id[0]][block_id[1]][1] = psm_world.pose.position.y
                self.block[block_id[0]][block_id[1]][2] = psm_world.pose.position.z
            
            elif self.state[i] == "release": 
                try:  
                    for i in range(np.shape(self.block)[0]):
                        for j in range(np.shape(self.block)[1]):
                            self.block[i][j][2] = self.z_mean
                except:
                    pass
                

    def state_callback(self, data):
        #FOR TEST
        self.actions += data.action_list
        if any([action for action in data.action_list if action.action == "grasp"]): #record pc after grasping (tissue manipulation started)
            self.cloud_time = rospy.Time.now()
            self.cloud_array = PC2Array()
            self.psm1_poses = PoseArray()
            self.psm2_poses = PoseArray()
        #FOR TEST

        self.state = []
        self.location = []
        self.manip_id = []
        self.block_id = []
        for i in range(len(data.action_list)):
            self.state.append(data.action_list[i].action)
            self.location.append(data.action_list[i].object)
            self.manip_id.append(data.action_list[i].robot)
            self.block_id.append(data.action_list[i].color)

            if self.state[i] == "move_ROI": #last possible option to uncover ROI
                self.discarded.append(self.block_id[i])

    def tissue_cb(self, data):
        if rospy.Time.now() - self.cloud_time > rospy.Duration(1.) or self.cloud_array.pc2_arr == []:
            self.cloud_array.pc2_arr.append(data)
            self.psm1_poses.poses.append(pm.toMsg(self.psm1.get_current_position()))
            self.psm2_poses.poses.append(pm.toMsg(self.psm2.get_current_position()))
            self.cloud_time = rospy.Time.now()

        if self.tissue_pts == []:
            gen = pc2.read_points(data, skip_nans=True, field_names=["x", "y", "z"])
            for point in gen:
                self.tissue_pts.append(point)
            self.received_tissue = True
        #COMPUTE CENTER OF TISSUE
        if self.tissue_center == PoseStamped():
            for point in self.tissue_pts:
                self.tissue_center.pose.position.x += point[0]
                self.tissue_center.pose.position.y += point[1]
                self.tissue_center.pose.position.z += point[2]
            self.tissue_center.pose.position.x /= len(self.tissue_pts)
            self.tissue_center.pose.position.y /= len(self.tissue_pts)
            self.tissue_center.pose.position.z /= len(self.tissue_pts)
            self.tissue_center.header = data.header

    def fixed_cb(self, data):
        if self.fixed_pts == []:
            gen = pc2.read_points(data, skip_nans=True, field_names=["x", "y", "z"])
            for point in gen:
                self.fixed_pts.append(point)
            self.received_fixed = True
        
    def gen_blocks(self):
        #COMPUTE VERTICES OF BLOCKS
        tissue_pts = self.tissue_pts
        x_tissue = [p[0] for p in tissue_pts]
        y_tissue = [p[1] for p in tissue_pts]
        z_tissue = [p[2] for p in tissue_pts]
        step_x = (max(x_tissue) - min(x_tissue)) / self.dim_grid_x
        x_vert_blocks = [min(x_tissue) + i*step_x for i in range(self.dim_grid_x+1)]
        step_y = (max(y_tissue) - min(y_tissue)) / self.dim_grid_y

        y_vert_blocks = [min(y_tissue) + i*step_y for i in range(self.dim_grid_y+1)]
        self.z_mean = np.mean(z_tissue) #axis of thickness, not used for block computation
        if self.block.all() == None:
            #ASSIGN POINTS TO CLUSTERS
            clust_pts = np.empty(self.dim_grid_x*self.dim_grid_y, np.object)
            for i in enumerate(clust_pts):
                clust_pts[i] = [[0., 0., self.z_mean, 0.]]
            clust_pts = np.reshape(clust_pts, (self.dim_grid_x, self.dim_grid_y))
            for p in tissue_pts:
                for i in range(self.dim_grid_x):
                    for j in range(self.dim_grid_y):
                        if p[0] >= x_vert_blocks[i] and p[0] < x_vert_blocks[i+1] and p[1] >= y_vert_blocks[j] and p[1] < y_vert_blocks[j+1]:
                            clust_pts[i][j][0] += p[0]
                            clust_pts[i][j][1] += p[1]
                            clust_pts[i][j][3] += 1.
            #COMPUTE CENTROIDS OF BLOCKS
            for i in range(self.dim_grid_x):
                for j in range(self.dim_grid_y):
                    try:
                        clust_pts[i][j] = [clust_pts[i][j][0] / clust_pts[i][j][3], clust_pts[i][j][1] / clust_pts[i][j][3], clust_pts[i][j][2]]
                    except Exception as e:
                        rospy.logwarn(str(e))
                        clust_pts[i][j] = [clust_pts[i][j][0], clust_pts[i][j][1], clust_pts[i][j][2]]
            self.block = clust_pts
        #ASSIGN FIXED POINTS TO CLUSTERS
        fixed_pts = self.fixed_pts
        self.fixed = [False for _ in range(self.dim_grid_y*self.dim_grid_x)] #[y, z, # points]
        self.fixed = np.reshape(self.fixed, (self.dim_grid_x, self.dim_grid_y))
        for p in fixed_pts:
            for i in range(self.dim_grid_x):
                for j in range(self.dim_grid_y):
                    if p[0] >= x_vert_blocks[i] and p[0] < x_vert_blocks[i+1] and p[1] >= y_vert_blocks[j] and p[1] < y_vert_blocks[j+1]:
                        self.fixed[i][j] = True

    def reach_target(self, idx):
        #SELECT TARGET BLOCK
        block_id_str = self.block_id[idx].replace("(", "").replace(")", "").replace(" ", "")
        comma = block_id_str.find(",")
        block_id = (int(block_id_str[0:comma])-1, int(block_id_str[(comma+1):])-1)
        #ORIENTATION
        if self.manip_id[idx] == "psm1":
            psm = self.psm1
        else:
            psm = self.psm2
        pose_cur = pm.toMsg(psm.get_current_position())
        pose_st_cur = PoseStamped(header = Header(frame_id = self.manip_id[idx].upper()+"_base", stamp = rospy.Time(0.)), pose = pose_cur)
        self.tf_list.waitForTransform(rospy.get_param("fixed_frame"), self.manip_id[idx].upper()+"_base", time = pose_st_cur.header.stamp, timeout = rospy.Duration(10.))
        pose_st_world = self.tf_list.transformPose(rospy.get_param("fixed_frame"), pose_st_cur)
        rot_world = pm.fromMsg(pose_st_world.pose).M #align to world for now. M is the rotation to apply to the pose to get the world frame
        init = rot_world.GetQuaternion()
        rot_world.DoRotX(math.pi) #to point down
        quat_rot = rot_world.GetQuaternion()
        quat_world = rotate(quat_rot, init)

        #COMMANDING ORIENTATION
        # return PoseStamped(header = Header(frame_id = rospy.get_param("fixed_frame"), stamp = rospy.Time.now()), 
        #                     pose = Pose(position = Point(x = self.block[block_id[0]][block_id[1]][0],
        #                                                     y = self.block[block_id[0]][block_id[1]][1],
        #                                                     z = self.block[block_id[0]][block_id[1]][2]), 
        #                                 orientation = Quaternion(x = quat_world[0],
        #                                                     y = quat_world[1],
        #                                                     z = quat_world[2],
        #                                                     w = quat_world[3])))

        #FIXED ORIENTATION
        return PoseStamped(header = Header(frame_id = rospy.get_param("fixed_frame"), stamp = rospy.Time.now()), 
                            pose = Pose(position = Point(x = self.block[block_id[0]][block_id[1]][0],
                                                            y = self.block[block_id[0]][block_id[1]][1],
                                                            z = self.block[block_id[0]][block_id[1]][2]), 
                                        orientation = pose_st_world.pose.orientation))

    def move_ROI_target(self, idx):
        if self.manip_id[idx] == "psm1":
            psm = self.psm1
        elif self.manip_id[idx] == "psm2":
            psm = self.psm2
        #TRANSFORM TO PSM BASE FRAME
        pose_base = pm.toMsg(psm.get_current_position())
        pose_base = PoseStamped(header = Header(frame_id=self.manip_id[idx].upper()+'_base' + self.dvrk_frame_id, stamp=rospy.Time(0.)), pose=pose_base)
        self.tf_list.waitForTransform(rospy.get_param("fixed_frame"), self.manip_id[idx].upper()+'_base' + self.dvrk_frame_id, time=pose_base.header.stamp, timeout = rospy.Duration(10.))
        pose_world = self.tf_list.transformPose(rospy.get_param("fixed_frame"), pose_base)

        #TARGET IS ROI
        return PoseStamped(header = Header(frame_id = rospy.get_param("fixed_frame"), stamp = rospy.Time.now()), 
                            pose = Pose(position = Point(x = self.roi_pose.pose.position.x,
                                                            y = self.roi_pose.pose.position.y, 
                                                            z = pose_world.pose.position.z)))
    def move_away_target(self, idx):
        if self.manip_id[idx] == "psm1":
            psm = self.psm1
        elif self.manip_id[idx] == "psm2":
            psm = self.psm2
        #TRANSFORM TO PSM BASE FRAME
        pose_base = pm.toMsg(psm.get_current_position())
        pose_base = PoseStamped(header = Header(frame_id=self.manip_id[idx].upper()+'_base' + self.dvrk_frame_id, stamp=rospy.Time(0.)), pose=pose_base)
        self.tf_list.waitForTransform(rospy.get_param("fixed_frame"), self.manip_id[idx].upper()+'_base' + self.dvrk_frame_id, time=pose_base.header.stamp, timeout = rospy.Duration(10.))
        pose_world = self.tf_list.transformPose(rospy.get_param("fixed_frame"), pose_base)

        #TARGET IS CENTER OF TISSUE
        return PoseStamped(header = Header(frame_id = rospy.get_param("fixed_frame"), stamp = rospy.Time.now()), 
                            pose = Pose(position = Point(x = self.tissue_center.pose.position.x,
                                                            y = self.tissue_center.pose.position.y, 
                                                            z = pose_world.pose.position.z)))
    def compute_target(self):  
        target = PoseStampedArray()
        for i in range(len(self.state)):
            if self.state[i] == "reach":
                target.poses.append(self.reach_target(i))
                self.test_target_pub.publish(target.poses[-1])
            elif self.state[i] == "move_ROI":
                target.poses.append(self.move_ROI_target(i))
            elif self.state[i] == "move_away":
                target.poses.append(self.move_away_target(i))

        self.target_pose_pub.publish(target)

    def compute_fluents(self):
        fluents = []
        # distances = []

        #COMPUTE HEIGHT OF PSMs
        psm1_base = PoseStamped(header = Header(stamp = rospy.Time(0.), frame_id = "PSM1_base"),
                                    pose = pm.toMsg(self.psm1.get_current_position()))
        self.tf_list.waitForTransform(rospy.get_param("fixed_frame"), "PSM1_base"+self.dvrk_frame_id, time=psm1_base.header.stamp, timeout=rospy.Duration(10.))
        psm1_world = self.tf_list.transformPose(rospy.get_param("fixed_frame"), psm1_base)
        psm2_base = PoseStamped(header = Header(stamp = rospy.Time(0.), frame_id = "PSM2_base"),
                                    pose = pm.toMsg(self.psm2.get_current_position()))
        self.tf_list.waitForTransform(rospy.get_param("fixed_frame"), "PSM2_base"+self.dvrk_frame_id, time=psm2_base.header.stamp, timeout=rospy.Duration(10.))
        psm2_world = self.tf_list.transformPose(rospy.get_param("fixed_frame"), psm2_base)

        #TO NOTIFY THAT MOVE_AWAY WAS EXECUTED
        if np.linalg.norm([psm1_world.pose.position.x - self.tissue_center.pose.position.x,
                            psm1_world.pose.position.y - self.tissue_center.pose.position.y]) < 0.005:
            fluents.append("at_center(psm1)")
        if np.linalg.norm([psm2_world.pose.position.x - self.tissue_center.pose.position.x,
                            psm2_world.pose.position.y - self.tissue_center.pose.position.y]) < 0.005:
            fluents.append("at_center(psm2)")

        #CHANGED GRID SIZE AFTER FIRST UNSAT
        if rospy.get_param("grid_size") != self.grid_size:
            self.grid_size = rospy.get_param("grid_size")
            self.dim_grid_x = rospy.get_param("grid_size")
            self.dim_grid_y = rospy.get_param("grid_size")

            #GENERATE ATOMS FOR BLOCKS
            rospy.sleep(2.)
            self.fixed_pts = []
            # self.tissue_pts = []
            self.block = np.empty(self.dim_grid_x*self.dim_grid_y, np.object) #ID of the block as a matrix of 3D centroids
            self.fixed = []
            self.received_fixed = False
            init_time = rospy.Time.now()
            while not self.received_fixed:
                pass
            while self.block.all() == None or (True not in self.fixed and rospy.Time.now() - init_time < rospy.Duration(5.)):
                if rospy.is_shutdown():
                    break
                try:
                    self.gen_blocks()
                except Exception as e:
                    rospy.logwarn("RECEIVING EMPTY POINT CLOUDS?")
                    rospy.logwarn(str(e))

        #WHICH BLOCK IS ABOVE ROI
        while self.roi_pose == PoseStamped():
            if rospy.is_shutdown():
                break
            pass
        min_dist = 100.
        min_block = "(0,0)"
        for i in range(np.shape(self.block)[0]):
            for j in range(np.shape(self.block)[1]):
                dist = np.linalg.norm([self.roi_pose.pose.position.x - self.block[i][j][0],
                                        self.roi_pose.pose.position.y - self.block[i][j][1]]) 
                if dist < min_dist:
                    min_block = "(" + str(i+1) + "," + str(j+1) + ")"
                    min_dist = dist
        fluents.append("above_tumor(" + min_block + ")")

        #COMPUTE REACHABILITY
        for i in range(np.shape(self.block)[0]):
            for j in range(np.shape(self.block)[1]):
                if self.tissue_center.pose.position.y < self.block[i][j][1]:
                    fluents.append("reachable(psm1,block," + "(" + str(i+1) + "," + str(j+1) + "))")
                else:
                    fluents.append("reachable(psm2,block," + "(" + str(i+1) + "," + str(j+1) + "))")

        #gripper status
        if self.psm2.get_current_jaw_position() < math.pi / 8.:
            fluents.append("closed_gripper(psm2)")
        if self.psm1.get_current_jaw_position() < math.pi / 8.:
            fluents.append("closed_gripper(psm1)")

        #POSITION OF ARMS WITH RESPECT TO TISSUE
        min_dist1 = 100.
        min_dist2 = 100.
        at1 = ""
        in_hand1 = ""
        at2 = ""
        in_hand2 = ""
        for i in range(np.shape(self.block)[0]):
            for j in range(np.shape(self.block)[1]):
                #PSM1
                dist = np.linalg.norm([psm1_world.pose.position.x - self.block[i][j][0],
                                        psm1_world.pose.position.y - self.block[i][j][1],
                                        psm1_world.pose.position.z - self.block[i][j][2]])
                if dist < min_dist1 and dist < 0.005:
                    min_dist1 = dist
                    if self.psm1.get_current_jaw_position() < math.pi / 8.:
                        in_hand1 = "in_hand(psm1,block,(" + str(i+1) + "," + str(j+1) + "))"

                        if psm1_world.pose.position.z > self.max_height or self.max_height_1:
                            fluents.append("max_height(psm1)")
                            self.max_height_1 = False
                    at1 = "at(psm1,block,(" + str(i+1) + "," + str(j+1) + "))"

                #PSM2
                dist = np.linalg.norm([psm2_world.pose.position.x - self.block[i][j][0],
                                        psm2_world.pose.position.y - self.block[i][j][1],
                                        psm2_world.pose.position.z - self.block[i][j][2]])
                if dist < min_dist2 and dist < 0.005:
                    min_dist2 = dist
                    if self.psm2.get_current_jaw_position() < math.pi / 8.:
                        in_hand2 = "in_hand(psm2,block,(" + str(i+1) + "," + str(j+1) + "))"
        
                        if psm2_world.pose.position.z > self.max_height or self.max_height_2:
                            fluents.append("max_height(psm2)")
                            self.max_height_2 = False
                    at2 = "at(psm2,block,(" + str(i+1) + "," + str(j+1) + "))"
        
        loc_fluents = [at1, at2, in_hand1, in_hand2]
        for f in loc_fluents:
            if f != "":
                fluents.append(f)
        
        if self.test:
            #FOR TEST
            if self.visible >= self.visible_thr or self.unstable or len([a for a in self.actions if a.action == "move_ROI" or a.action == "move_away"]) >= 2 or any([d for d in self.discarded if d in self.block_id]): #SINCE NOT CONSIDERING FIXED BLOCKS, AVOID ENDLESS FAULTY MOTION WITH FIXED ON THE WAY
                for i in range(len(self.block_id)):
                    self.discarded.append(self.block_id[i])
                fluents.append("max_height(psm1)")
                fluents.append("max_height(psm2)")
                duration = rospy.Time.now() - self.init_time
                self.init_time = rospy.Time.now()
                self.time_pub.publish(Float32(duration.to_sec()))
                self.distance_pub.publish(Float32(self.distance))
                self.force_pub.publish(Flt_arr(data = self.max_forces))
                self.stress_pub.publish(Flt_arr(data = self.stress_list))
                self.visibility_pub.publish(Float32(self.visible))
                self.actions_pub.publish(ActionArray(self.actions))
                self.cloud_pub.publish(self.cloud_array)
                self.psm1_pub.publish(self.psm1_poses)
                self.psm2_pub.publish(self.psm2_poses)
                self.force_fail_pub.publish(self.force_fail_times)
                self.actions = []
                self.max_forces = []
                self.unstable = False
                self.distance = 0.
                self.force_fail_times = 0
                self.psm1_pose_start = list(self.psm1.get_current_position().p)
                self.psm2_pose_start = list(self.psm2.get_current_position().p)
                self.end_pub.publish(Bool(True))
                rospy.sleep(60.) #GIVE SOME PAUSE TO ENSURE THAT FOLLOWING PLANS ARE CAPTURED
            #FOR TEST
        else:
            #GENERATE ATOMS FOR FIXED BLOCKS
            rospy.sleep(2.)
            self.fixed_pts = []
            # self.tissue_pts = []
            self.fixed = []
            self.received_fixed = False
            init_time = rospy.Time.now()
            while not self.received_fixed:
                pass
            while (True not in self.fixed and rospy.Time.now() - init_time < rospy.Duration(5.)):
                if rospy.is_shutdown():
                    break
                try:
                    self.gen_blocks()
                except Exception as e:
                    rospy.logwarn("RECEIVING EMPTY POINT CLOUDS?")
                    rospy.logwarn(str(e))

            #visibility of tumor
            if self.visible >= self.visible_thr or rospy.Time.now() - self.init_time > rospy.Duration(300.) or self.unsat or self.unstable:
                if self.visible >= self.visible_thr:
                    fluents.append("visible_tumor")
                
                duration = rospy.Time.now() - self.init_time
                self.init_time = rospy.Time.now()
                self.time_pub.publish(Float32(duration.to_sec()))
                self.distance_pub.publish(Float32(self.distance))
                self.force_pub.publish(Flt_arr(data = self.max_forces))
                self.stress_pub.publish(Flt_arr(data = self.stress_list))
                self.monitoring_pub.publish(Flt_arr(data = self.monitoring_arr))
                self.visibility_pub.publish(Float32(self.visible))
                self.actions_pub.publish(ActionArray(self.actions))
                self.cloud_pub.publish(self.cloud_array)
                self.psm1_pub.publish(self.psm1_poses)
                self.psm2_pub.publish(self.psm2_poses)
                self.force_fail_pub.publish(self.force_fail_times)
                self.actions = []
                self.monitoring_arr = []
                self.max_forces = []
                self.unstable = False
                self.unsat = False
                self.distance = 0.
                self.force_fail_times = 0
                self.psm1_pose_start = list(self.psm1.get_current_position().p)
                self.psm2_pose_start = list(self.psm2.get_current_position().p)
                self.end_pub.publish(Bool(True))

        #FIXED BLOCKS
        where_fixed = np.where(self.fixed == True)
        for i in range(np.shape(where_fixed[0])[0]):
            fluents.append("fixed((" + str(where_fixed[0][i] + 1) + "," + str(where_fixed[1][i] + 1) + "))")

        #IF CURRENT BLOCK IS USELESS FOR EXPOSING ROI, DISCARD IT FOR RE-PLANNING
        for block in self.discarded:
            fluents.append("discarded(" + block + ")")
        
        #SEND FLUENTS
        msg = ContextModel()
        for i in range(len(fluents)):
           msg.atoms.append(fluents[i])
        self.fluent_pub.publish(msg)
        self.request = 0
        self.fluent_comp_status_pub.publish(Bool(True))
        rospy.sleep(1.)

    def reset(self):
        self.failure = False
        self.warning = False
        self.monitoring = 0
        self.request = 0
    
    # FORCE-BASED FAILURE
    def force_failure(self):
        if self.max_force > self.force_thr:
            rospy.logwarn("MAX FORCE IS " + str(self.max_force))
            if self.warning:
                rospy.logwarn("SLOWING DOWN (FORCE TOLERANCE)")
                if rospy.Time.now() - self.init_warning_time > self.warning_time:
                    self.force_fail_times += 1
                    self.failure = True
                    self.warning = False
                    rospy.logwarn("INTERRUPTING (FORCE TOLERANCE)")

                    for i in range(len(self.manip_id)):
                        if self.state[i] == "pull":
                            if self.manip_id[i] == "psm1":
                                self.max_height_1 = True
                            else:
                                self.max_height_2 = True
                        elif self.block_id[i] != "none":
                            self.discarded.append(self.block_id[i])
                    self.max_force = 0.
                    self.init_warning_time = rospy.Time.now()
            else:
                self.force_fail_times += 1
                self.warning = True
                self.init_warning_time = rospy.Time.now()
                self.warn_pub.publish(Bool(self.warning))
    
    # POSITION-BASED FAILURE
    def failure_pull(self, idx):
        if self.manip_id[idx] == "psm1":
            psm = self.psm1
        else:
            psm = self.psm2
        
        pose_base = pm.toMsg(psm.get_current_position())
        pose_st_base = PoseStamped(header=Header(frame_id=psm.name().upper()+"_base", stamp=rospy.Time(0.)), pose=pose_base)
        self.tf_list.waitForTransform(rospy.get_param("fixed_frame"), psm.name().upper()+"_base"+self.dvrk_frame_id, time=pose_st_base.header.stamp, timeout=rospy.Duration(10.))
        pose_world = self.tf_list.transformPose(rospy.get_param("fixed_frame"), pose_st_base)
        if pose_world.pose.position.z > self.max_height:
            self.failure = True
            rospy.logwarn("INTERRUPTING (MAX HEIGHT)")
            if self.manip_id[idx] == "psm1":
                self.max_height_1 = True
            else:
                self.max_height_2 = True

    def check_failure(self):
        #FORCE FAILURE
        self.force_failure()
        #POSITION FAILURE
        for i in range(len(self.state)):
            if self.state[i] == "pull":
                self.failure_pull(i)

            







def main():

    rospy.init_node('sensing_node')
    sa = Situation_awareness()
    rospy.sleep(1.)
    psm1_pos = sa.psm1.get_current_position().p
    psm2_pos = sa.psm2.get_current_position().p

    while not rospy.is_shutdown():
        # TESTING 
        # if sa.block != []:
        #     arr = PoseArray()
        #     arr.header.stamp = rospy.Time.now()
        #     arr.header.frame_id = rospy.get_param("fixed_frame")
        #     for i in range(np.shape(sa.block)[0]):
        #         for j in range(np.shape(sa.block)[1]):
        #             arr.poses.append(Pose(position = Point(x = sa.block[i][j][0], y = sa.block[i][j][1], z = sa.block[i][j][2])))
        #     sa.test_pub.publish(arr)

        if sa.request == 1:
            sa.compute_fluents()
        elif sa.request == 2:
            sa.compute_target()
            sa.check_failure()
            if sa.failure or sa.unstable or sa.monitoring >= 2:
                if sa.monitoring >= 2:
                    rospy.logwarn("MONITORING THRESHOLD 2: REPLANNING")
                    #DISCARD IF MONITORING ABOVE THRESHOLD TOO OFTEN (INSTRUMENTS UNABLE TO MOVE)
                    if np.linalg.norm([sa.psm1.get_current_position().p[0] - psm1_pos[0],sa.psm1.get_current_position().p[1] - psm1_pos[1],sa.psm1.get_current_position().p[2] - psm1_pos[2]]) < 0.002 and np.linalg.norm([sa.psm2.get_current_position().p[0] - psm2_pos[0],sa.psm2.get_current_position().p[1] - psm2_pos[1],sa.psm2.get_current_position().p[2] - psm2_pos[2]]) < 0.002:
                        for i in range(len(sa.block_id)):
                            sa.discarded.append(sa.block_id[i])

                sa.reset()
                psm1_pos = sa.psm1.get_current_position().p
                psm2_pos = sa.psm2.get_current_position().p
                sa.failure_pub.publish(Bool(True))
            if sa.monitoring == 3:
                rospy.logwarn("MONITORING THRESHOLD 3: STOPPING TASK")
                try:
                    os.system("rosnode kill control_node")
                    os.system("ps -ef | grep 'control.py' | grep -v grep | awk '{print $2}' | xargs -r kill -9")       
                except:
                    rospy.logwarn("ALREADY KILLED control")
                

    rospy.spin()







if __name__ == '__main__':
    main()


