import numpy as np
import math
import os
import rospy
import copy
import signal
from dvrk_task_msgs.msg import ObstArray, ActionArray, PoseStampedArray
from std_msgs.msg import Header, Int32, Bool
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion
from tf import TransformListener
from tf_conversions import posemath as pm
from tf.transformations import quaternion_slerp as q_interp
import dvrk
import PyKDL













#MOTION CONTROL
class motion_manager(object):
    def __init__(self):
        self.dvrk_frame_id  = rospy.get_param("dvrk_frame_id")

        self.psm1 = dvrk.psm("PSM1")
        self.psm2 = dvrk.psm("PSM2")
        self.agents = []
        self.n_arms = [] #for dual-arm actions
        self.policies = []
        self.policy_types = []
        self.fixed_obst = True
        self.obstacles = ObstArray()
        self.target_pose = PoseStampedArray() 
        self.tissue_center = PoseStamped()
        self.state = [] #ASP action
        self.manip_id = [] #the name of the manipulator to be used to perform the prescribed action
        self.failure = False
        self.warning = False
        self.fluents = []
        self.location = []
        self.monitoring = 0
        self.computed_fluents = False
        self.target_pose_listener = rospy.Subscriber(rospy.get_param("target_topic"), PoseStampedArray, self.target_pose_callback)
        self.state_listener = rospy.Subscriber(rospy.get_param("action_topic"), ActionArray, self.state_callback)
        self.failure_listener = rospy.Subscriber(rospy.get_param("failure_topic"), Bool, self.failure_cb)
        self.warning_listener = rospy.Subscriber(rospy.get_param("warning_topic"), Bool, self.warning_cb)
        self.computed_fluent_status_listener = rospy.Subscriber(rospy.get_param("fluent_status_topic"), Bool, self.computed_fluents_cb)
        self.tf_list = TransformListener()
        self.end_pub = rospy.Publisher(rospy.get_param("action_feedback_topic"), Bool, queue_size=1)
        self.sensing_pub = rospy.Publisher(rospy.get_param("sensing_request_topic"), Int32, queue_size=1)
        self.obst_sub = rospy.Subscriber(rospy.get_param("obstacles_topic"), ObstArray, self.obstacles_callback)
        self.tissue_center_sub = rospy.Subscriber(rospy.get_param("tissue_center_topic"), PoseStamped, self.tissue_center_cb)
        self.monitoring_sub = rospy.Subscriber(rospy.get_param("uncertainty_topic"), Int32, self.monitoring_cb)

        self.motion_status_pub = rospy.Publisher(rospy.get_param("motion_status_topic"), Bool, queue_size=1)
        self.control_pub = rospy.Publisher("started_control", Bool, queue_size=1)
        rospy.sleep(1.)

        # rospy.on_shutdown(self.on_shutdown)

        # Home positions for the dvrk arms
        home_psm1_world = (0.03,0.06,0.07)
        home_psm2_world = (0.03,-0.06,0.07)
        self.home_psm1_base = self.fixedframe2base(home_psm1_world, "PSM1")
        self.home_psm2_base = self.fixedframe2base(home_psm2_world, "PSM2")
        width = math.pi/2.
        self.psm1.move_jaw(width)
        self.psm2.move_jaw(width)

        self.motion_rate = 5.

        #RETRACTION SPECIFIC
        self.custom_policies = { "pull" : self.pull,
                                 "grasp" : self.grasp,
                                 "release" : self.release,
                                 "move_ROI" : self.move_ROI,
                                 "move_away" : self.move_away,
                                 "reach" : self.reach
                               } 

        self.move_to_start()

    def on_shutdown(self):
        os.system("rosnode kill control_node")
        os.system("ps -ef | grep 'control.py' | grep -v grep | awk '{print $2}' | xargs -r kill -9")
        os.kill(os.getpid(), signal.SIGKILL)

    def failure_cb(self, data):
        self.failure = data.data
    
    def monitoring_cb(self, data):
        self.monitoring = data.data

    def warning_cb(self, data):
        self.warning = data.data

    def computed_fluents_cb(self, data):
        self.computed_fluents = data.data
    
    def tissue_center_cb(self, data):
        self.tissue_center = data

    def target_pose_callback(self, data):
        self.target_pose = copy.deepcopy(data)

    def obstacles_callback(self, data):  
        self.obstacles = data   

    def state_callback(self, data):
        for i in range(len(data.action_list)):
            self.location.append(data.action_list[i].object)
            self.manip_id.append(data.action_list[i].robot)
            self.state.append(data.action_list[i].action)
    
    def move_to_start(self):
        discretization = 20
        rate = rospy.Rate(self.motion_rate)
        start1 = self.psm1.get_current_position().p
        start2 = self.psm2.get_current_position().p

        end1 = self.home_psm1_base
        end2 = self.home_psm2_base

        step1 = (end1 - start1) / discretization
        step2 = (end2 - start2) / discretization
        for i in range(discretization):
            self.psm1.move(PyKDL.Vector(start1[0] + i*step1[0], start1[1] + i*step1[1], start1[2] + i*step1[2]), blocking=False)
            self.psm2.move(PyKDL.Vector(start2[0] + i*step2[0], start2[1] + i*step2[1], start2[2] + i*step2[2]), blocking=False)
            rate.sleep()

        self.control_pub.publish(Bool(True))

    def pull(self, idx):
        height = 0.01
        discretization = 20

        #ASK SENSING FOR TARGET AND ANOMALIES
        self.sensing_pub.publish(Int32(2))
        rospy.sleep(2.)

        if self.agents[idx] == "psm1":
            psm = self.psm1
        elif self.agents[idx] == "psm2":
            psm = self.psm2

        rate = rospy.Rate(self.motion_rate)
        start_base = pm.toMsg(psm.get_current_position())
        start_base = PoseStamped(header=Header(frame_id=self.agents[idx].upper()+'_base', stamp=rospy.Time(0.)), pose=start_base)
        self.tf_list.waitForTransform(rospy.get_param("fixed_frame"), self.agents[idx].upper()+'_base' + self.dvrk_frame_id, time=start_base.header.stamp, timeout = rospy.Duration(5.))
        start_world = self.tf_list.transformPose(rospy.get_param("fixed_frame"), start_base)
        target_world = start_world
        for _ in range(discretization):
            target_world.pose.position.z += height/discretization
            target_base = self.tf_list.transformPose(self.agents[idx].upper()+'_base', target_world)
            psm.move(PyKDL.Vector(target_base.pose.position.x, target_base.pose.position.y, target_base.pose.position.z), blocking=False)
            if self.failure:
                break
            elif self.warning or self.monitoring == 1:
                rate = rospy.Rate(self.motion_rate/2.)
            rate.sleep()
        
    def grasp(self, idx):
        if self.agents[idx] == "psm1":
            psm = self.psm1
        elif self.agents[idx] == "psm2":
            psm = self.psm2

        psm.close_jaw()

    def release(self, idx):
        height = 0.0
        discretization = 10
        width = math.pi/2.
        if self.agents[idx] == "psm1":
            psm = self.psm1
        elif self.agents[idx] == "psm2":
            psm = self.psm2
        psm.move_jaw(width)
       
        rate = rospy.Rate(self.motion_rate)
        start_base = pm.toMsg(psm.get_current_position())
        start_base = PoseStamped(header=Header(frame_id=self.agents[idx].upper()+'_base', stamp=rospy.Time(0.)), pose=start_base)
        self.tf_list.waitForTransform(rospy.get_param("fixed_frame"), self.agents[idx].upper()+'_base' + self.dvrk_frame_id, time=start_base.header.stamp, timeout = rospy.Duration(5.))
        start_world = self.tf_list.transformPose(rospy.get_param("fixed_frame"), start_base)
        target_world = start_world
        for _ in range(discretization):
            target_world.pose.position.z += height/discretization
            target_base = self.tf_list.transformPose(self.agents[idx].upper()+'_base', target_world)
            psm.move(PyKDL.Vector(target_base.pose.position.x, target_base.pose.position.y, target_base.pose.position.z), blocking=False)
            rate.sleep()
        
        self.move_to_start()
    
    def move_ROI(self, idx):
        discretization = 40 #to avoid fast motion

        #ASK SENSING FOR TARGET AND ANOMALIES
        self.sensing_pub.publish(Int32(2))
        rospy.sleep(2.)
        while len(self.target_pose.poses) <= idx:
            pass
        #SELECT ARM
        if self.agents[idx] == "psm1":
            psm = self.psm1
        elif self.agents[idx] == "psm2":
            psm = self.psm2
        #TRANSFORM TO PSM BASE FRAME
        self.target_pose.poses[idx].header.stamp = rospy.Time(0.)
        self.tf_list.waitForTransform(rospy.get_param("fixed_frame"), self.agents[idx].upper()+'_base' + self.dvrk_frame_id, time=self.target_pose.poses[idx].header.stamp, timeout = rospy.Duration(5.))
        target = self.tf_list.transformPose(self.agents[idx].upper()+'_base' + self.dvrk_frame_id, self.target_pose.poses[idx])
        #COMPUTE DIFF
        start = PyKDL.Vector(psm.get_current_position().p[0], psm.get_current_position().p[1], psm.get_current_position().p[2])
        end = PyKDL.Vector(target.pose.position.x, target.pose.position.y, target.pose.position.z)
        diff = end - start  
        #COMMAND DIFF  
        rate = rospy.Rate(self.motion_rate)
        step = 1./discretization
        for i in range(discretization):
            psm.move(start + diff*step*(i+1), blocking = True)
            if self.failure:
                break
            elif self.warning or self.monitoring == 1:
                rate = rospy.Rate(self.motion_rate/5.)
            rate.sleep()
    
    def move_away(self, idx):
        discretization = 40 #to avoid fast motion

        #ASK SENSING FOR TARGET AND ANOMALIES
        self.sensing_pub.publish(Int32(2))
        rospy.sleep(2.)
        while len(self.target_pose.poses) <= idx:
            pass
        #SELECT ARM
        if self.agents[idx] == "psm1":
            psm = self.psm1
        elif self.agents[idx] == "psm2":
            psm = self.psm2
        #TRANSFORM TO PSM BASE FRAME
        self.target_pose.poses[idx].header.stamp = rospy.Time(0.)
        self.tf_list.waitForTransform(rospy.get_param("fixed_frame"), self.agents[idx].upper()+'_base' + self.dvrk_frame_id, time=self.target_pose.poses[idx].header.stamp, timeout = rospy.Duration(5.))
        target = self.tf_list.transformPose(self.agents[idx].upper()+'_base' + self.dvrk_frame_id, self.target_pose.poses[idx])
        #COMPUTE DIFF
        start = PyKDL.Vector(psm.get_current_position().p[0], psm.get_current_position().p[1], psm.get_current_position().p[2])
        end = PyKDL.Vector(target.pose.position.x, target.pose.position.y, target.pose.position.z)
        diff = end - start  
        #COMMAND DIFF  
        rate = rospy.Rate(self.motion_rate)
        step = 1./discretization
        for i in range(discretization): #move a little bit more than the ROI, to expose it better
            psm.move(start + diff*step*(i+1), blocking = True)
            if self.failure:
                break
            elif self.warning or self.monitoring == 1:
                rate = rospy.Rate(self.motion_rate/5.)
            rate.sleep()

    def reach(self, idx):
        discretization = 20 #to avoid fast motion

        #ASK SENSING FOR TARGET AND ANOMALIES
        self.sensing_pub.publish(Int32(2))
        rospy.sleep(2.)
        while len(self.target_pose.poses) <= idx:
            pass
        #SELECT ARM
        if self.agents[idx] == "psm1":
            psm = self.psm1
        elif self.agents[idx] == "psm2":
            psm = self.psm2
        #TRANSFORM TO PSM BASE FRAME
        self.target_pose.poses[idx].header.stamp = rospy.Time(0.)

        self.tf_list.waitForTransform(rospy.get_param("fixed_frame"), self.agents[idx].upper()+'_base' + self.dvrk_frame_id, time=self.target_pose.poses[idx].header.stamp, timeout = rospy.Duration(5.))
        target = self.tf_list.transformPose(self.agents[idx].upper()+'_base' + self.dvrk_frame_id, self.target_pose.poses[idx])
        #COMPUTE DIFF POSITION
        start_pos = np.array([psm.get_current_position().p[0], psm.get_current_position().p[1], psm.get_current_position().p[2]])
        end_pos = np.array([target.pose.position.x, target.pose.position.y, target.pose.position.z])
        step_pos = (end_pos - start_pos)/discretization
        #ORIENTATION
        start_or = [psm.get_current_position().M.GetQuaternion()[0], psm.get_current_position().M.GetQuaternion()[1], psm.get_current_position().M.GetQuaternion()[2], psm.get_current_position().M.GetQuaternion()[3]]
        end_or = [target.pose.orientation.x, target.pose.orientation.y, target.pose.orientation.z, target.pose.orientation.w]
        #COMMAND DIFF  
        rate = rospy.Rate(self.motion_rate)
        for i in range(discretization):
            next_or = q_interp(start_or, end_or, float(i+1)/discretization)
            pose = Pose(position = Point(x = start_pos[0] + (i+1)*step_pos[0], y = start_pos[1] + (i+1)*step_pos[1], z = start_pos[2] + (i+1)*step_pos[2]), 
                            orientation = Quaternion(x = next_or[0], y = next_or[1], z = next_or[2], w = next_or[3]))
            psm.move(pm.fromMsg(pose), blocking = True)
            rate.sleep()

    def fixedframe2base(self, fixedframe_position, psm_id):
        world_pose = PoseStamped()
        world_pose.header.frame_id      = rospy.get_param("fixed_frame")
        world_pose.header.stamp         = rospy.Time(0.)
        world_pose.pose.position.x      = fixedframe_position[0]
        world_pose.pose.position.y      = fixedframe_position[1]
        world_pose.pose.position.z      = fixedframe_position[2]
        world_pose.pose.orientation.x   = 0
        world_pose.pose.orientation.y   = 0
        world_pose.pose.orientation.z   = 0
        world_pose.pose.orientation.w   = 1

        self.tf_list.waitForTransform(psm_id+'_base'+self.dvrk_frame_id, rospy.get_param("fixed_frame"), time=rospy.Time(0.), timeout = rospy.Duration(5.))
        base_pose = self.tf_list.transformPose(psm_id+'_base'+self.dvrk_frame_id, world_pose)
        return PyKDL.Vector(base_pose.pose.position.x, base_pose.pose.position.y, base_pose.pose.position.z)

    def execute(self):
        if not self.failure:
            for i in range(len(self.policies)):
                self.custom_policies[self.policies[i]](i)

        self.motion_status_pub.publish(Bool(True))
        rospy.sleep(2.)

    def reset(self):
        self.failure = False
        self.warning = False
        self.monitoring = 0
        self.state = []
        self.n_arms = []
        self.policies = []
        self.policy_types = []
        self.agents = []
        self.location = []
        self.manip_id = []
