#!/usr/bin/env python2

import rospkg
import rospy
from std_msgs.msg import Bool, Empty, Float32, Float32MultiArray as FltArr, Int32

import os
import subprocess
import copy

from geometry_msgs.msg import PoseArray
from dvrk_task_msgs.msg import ActionArray, PC2Array, ContextModel

import numpy as np


class Listener(object):
    def __init__(self):
        self.path_pkg = rospkg.RosPack().get_path("robot_control")
        self.path_sofa = "runSofa"
        self.path_sim = os.path.join(self.path_pkg, '../simulation/simulation.py')
        self.path_sim_real = os.path.join(self.path_pkg, '../simulation/actual_simulation.py')
        self.plan_file   = self.path_pkg + "/results/plan_comparison/plan"

        self.received_data = False
        self.visible = 0.
        self.distance = 0.
        self.actions = []
        self.ended = False
        self.unsat = False
        # self.unsat = 1
        self.unstable = False
        self.force = []
        self.stress = []
        self.monitoring = []
        self.context = []
        self.time = 0.
        self.force_fail_times = 0
        self.psm1_poses = PoseArray()
        self.psm2_poses = PoseArray()
        self.cloud = PC2Array()
        self.power_on_pub = rospy.Publisher("/dvrk/console/power_on", Empty, queue_size=1)
        self.power_off_pub = rospy.Publisher("/dvrk/console/power_off", Empty, queue_size=1)
        self.home_pub = rospy.Publisher("/dvrk/console/home", Empty, queue_size=1)
        self.replan_pub = rospy.Publisher("/replan_test", Bool, queue_size=1)

        self.visible_listener = rospy.Subscriber("/visibility_test", Float32, self.visible_cb)
        self.ended_listener = rospy.Subscriber("/ended_test", Bool, self.ended_cb)
        self.unsat_listener = rospy.Subscriber("/unsat_test", Bool, self.unsat_cb)
        self.unstable_listener = rospy.Subscriber("/unstable_test", Bool, self.unstable_cb)
        self.force_listener = rospy.Subscriber("/force_test", FltArr, self.force_cb)
        self.stress_listener = rospy.Subscriber("/stress_test", FltArr, self.stress_cb)
        self.time_listener = rospy.Subscriber("/time_test", Float32, self.time_cb)
        self.distance_listener = rospy.Subscriber("/distance_test", Float32, self.distance_cb)
        self.actions_listener = rospy.Subscriber("/actions_test", ActionArray, self.action_cb)
        self.cloud_listener = rospy.Subscriber("cloud_test", PC2Array, self.cloud_cb)
        self.psm1_listener = rospy.Subscriber("psm1_test", PoseArray, self.psm1_cb)
        self.psm2_listener = rospy.Subscriber("psm2_test", PoseArray, self.psm2_cb)
        self.context_listener = rospy.Subscriber("/context/model", ContextModel, self.context_cb)
        self.monitoring_listener = rospy.Subscriber("monitoring_test", FltArr, self.monitoring_cb)
        self.force_fail_listener = rospy.Subscriber("force_fail_test", Int32, self.force_fail_cb)
    
    def ended_cb(self, data):
        self.ended = data.data
    
    def force_fail_cb(self, data):
        self.force_fail_times = data.data

    def unsat_cb(self, data):
        self.unsat = data.data

    def psm1_cb(self, data):
        self.psm1_poses = data

    def unstable_cb(self, data):
        self.unstable = data.data

    def psm2_cb(self, data):
        self.psm2_poses = data

    def cloud_cb(self, data):
        self.cloud = data
    
    def context_cb(self, data):
        self.context = data.atoms

    def force_cb(self, data):
        self.force = data.data

    def stress_cb(self, data):
        self.stress = data.data

    def monitoring_cb(self, data):
        self.monitoring = data.data
    
    def time_cb(self, data):
        self.time = data.data
    
    def distance_cb(self, data):
        self.distance = data.data
    
    def visible_cb(self, data):
        self.visible = data.data

    def action_cb(self, data):
        self.actions = data.action_list





  





def test_optimal(listener):    
    visible_list = []
    force_list = []
    stress_list = []
    time_list = []
    distance_list = []
    actions_list = []
    cloud_list = []
    unstable_list = []
    psm1_list = []
    psm2_list = []
    force_fail_list = []

    listener.power_on_pub.publish(Empty())
    rospy.sleep(5.)
    listener.home_pub.publish(Empty())
    rospy.sleep(5.)
    proc = subprocess.Popen([listener.path_sofa, listener.path_sim, "--g", "batch", "--nbIter", "100000000"])
    rospy.sleep(2.)
    proc1 = subprocess.Popen(["roslaunch", listener.path_pkg + "/launch/framework.launch"])

    while not listener.unsat:
        while not listener.ended and not listener.unsat:
            pass

        if listener.unsat:
            #WRITE DATA FOR SELECTED SCENARIO
            out_file = listener.path_pkg + "/results/optimal_test" + rospy.get_param("id_configuration")
            visible = np.array(visible_list)
            force = np.array(force_list)
            stress = np.array(stress_list)
            distance = np.array(distance_list)
            actions = np.array(actions_list)
            time = np.array(time_list)
            force_fail = np.array(force_fail_list)
            cloud = np.array(cloud_list)
            psm1 = np.array(psm1_list)
            psm2 = np.array(psm2_list)
            unstable = np.array(unstable_list)
            context = np.array(listener.context)
            np.savez(out_file, force_fail=force_fail, context=context, visible=visible, distance=distance, stress=stress, time=time, actions=actions, force=force, cloud=cloud, psm1=psm1, psm2=psm2, unstable=unstable) #label=value
            
            #SWITCH OFF CONSOLE
            listener.power_off_pub.publish(Empty())
            rospy.sleep(5.)

            #KILL SOFA
            proc.kill()
            proc.wait() 
            try:
                os.system("ps -ef | grep 'sofa' | grep -v grep | awk '{print $2}' | xargs -r kill -9")
            except:
                rospy.loginfo("ALREADY KILLED SOFA")

            #KILL ROS NODES
            try:
                os.system("rosnode kill ASPplanner")
                os.system("ps -ef | grep 'ASP_manager' | grep -v grep | awk '{print $2}' | xargs -r kill -9")
            except:
                rospy.loginfo("ALREADY KILLED ASPplanner")
            try:
                os.system("rosnode kill sensing_node")
                os.system("ps -ef | grep 'sensing' | grep -v grep | awk '{print $2}' | xargs -r kill -9")
            except:
                rospy.loginfo("ALREADY KILLED sensing")
            try:
                os.system("rosnode kill control_node")
                os.system("ps -ef | grep 'control.py' | grep -v grep | awk '{print $2}' | xargs -r kill -9")       
            except:
                rospy.loginfo("ALREADY KILLED control")
            proc1.kill()
            proc1.wait()

        elif listener.ended:
            rospy.sleep(5.) #ALLOW PUBLICATION OF DATA FROM SENSING
            visible_list.append(listener.visible)
            force_list.append(listener.force)
            stress_list.append(listener.stress)
            time_list.append(listener.time)
            distance_list.append(listener.distance)
            actions_list.append(listener.actions)
            cloud_list.append(listener.cloud)
            psm1_list.append(listener.psm1_poses)
            psm2_list.append(listener.psm2_poses)
            unstable_list.append(listener.unstable)
            force_fail_list.append(listener.force_fail_times)
            listener.unstable = False
            listener.ended = False
            listener.unsat = False

    #TERMINATE ALL 
    os.system("ps -ef | grep 'control' | grep -v grep | awk '{print $2}' | xargs -r kill -9") #dvrk 
    os.system("rosnode kill test_sofa") #this node















def test_optimal_real(listener):    
    visible_list = []
    force_list = []
    stress_list = []
    time_list = []
    distance_list = []
    actions_list = []
    cloud_list = []
    unstable_list = []
    psm1_list = []
    psm2_list = []
    force_fail_list = []

    listener.power_on_pub.publish(Empty())
    rospy.sleep(5.)
    listener.home_pub.publish(Empty())
    rospy.sleep(5.)
    proc = subprocess.Popen([listener.path_sofa, listener.path_sim, "--g", "batch", "--nbIter", "100000000"])
    rospy.sleep(2.)
    proc1 = subprocess.Popen(["roslaunch", listener.path_pkg + "/launch/framework.launch"])

    while not listener.unsat:
        while not listener.ended and not listener.unsat:
            pass

        if listener.unsat:
            #WRITE DATA FOR CURRENT SCENARIO
            out_file = listener.path_pkg + "/results/optimal_test_real" + rospy.get_param("id_configuration")[1:] + rospy.get_param("real_variant")
            visible = np.array(visible_list)
            force = np.array(force_list)
            stress = np.array(stress_list)
            distance = np.array(distance_list)
            actions = np.array(actions_list)
            time = np.array(time_list)
            cloud = np.array(cloud_list)
            psm1 = np.array(psm1_list)
            psm2 = np.array(psm2_list)
            force_fail = np.array(force_fail_list)
            unstable = np.array(unstable_list)
            context = np.array(listener.context)
            np.savez(out_file, force_fail=force_fail, context=context, visible=visible, distance=distance, time=time, actions=actions, force=force, cloud=cloud, psm1=psm1, psm2=psm2, unstable=unstable) #label=value
            
            #SWITCH OFF CONSOLE
            listener.power_off_pub.publish(Empty())
            rospy.sleep(5.)

            #KILL SOFA
            proc.kill()
            proc.wait() 
            try:
                os.system("ps -ef | grep 'sofa' | grep -v grep | awk '{print $2}' | xargs -r kill -9")
            except:
                rospy.loginfo("ALREADY KILLED SOFA")

            #KILL ROS NODES
            try:
                os.system("rosnode kill ASPplanner")
                os.system("ps -ef | grep 'ASP_manager' | grep -v grep | awk '{print $2}' | xargs -r kill -9")
            except:
                rospy.loginfo("ALREADY KILLED ASPplanner")
            try:
                os.system("rosnode kill sensing_node")
                os.system("ps -ef | grep 'sensing' | grep -v grep | awk '{print $2}' | xargs -r kill -9")
            except:
                rospy.loginfo("ALREADY KILLED sensing")
            try:
                os.system("rosnode kill control_node")
                os.system("ps -ef | grep 'control.py' | grep -v grep | awk '{print $2}' | xargs -r kill -9")       
            except:
                rospy.loginfo("ALREADY KILLED control")
            proc1.kill()
            proc1.wait()

        elif listener.ended:
            rospy.sleep(5.) #ALLOW PUBLICATION OF DATA FROM SENSING
            visible_list.append(listener.visible)
            force_list.append(listener.force)
            stress_list.append(listener.stress)
            time_list.append(listener.time)
            distance_list.append(listener.distance)
            actions_list.append(listener.actions)
            cloud_list.append(listener.cloud)
            psm1_list.append(listener.psm1_poses)
            psm2_list.append(listener.psm2_poses)
            unstable_list.append(listener.unstable)
            force_fail_list.append(listener.force_fail_times)
            listener.unstable = False
            listener.ended = False
            listener.unsat = False
    

    #TERMINATE ALL 
    os.system("ps -ef | grep 'control' | grep -v grep | awk '{print $2}' | xargs -r kill -9") #dvrk 
    os.system("rosnode kill test_sofa") #this node
























def normalize(my_list, min=0., max=1.):
    '''Normalize a list between min and max'''
    if min == 0.:
        min = np.min(my_list)
    if max == 1.:
        max = np.max(my_list)
    if np.max(my_list) - np.min(my_list) > 1e-9:
        return list((np.array(my_list) - np.ones(len(my_list)) * min) / (max - min))
    else:
        return list(np.array(my_list) / max(my_list))



















def best_plan(listener):
    file = listener.path_pkg + "/results/optimal_test" + rospy.get_param("id_configuration") + ".npz"
    data = np.load(file)
    force = list(data['force'])
    actions = list(data['actions'])
    distance = list(data['distance'])
    visible = list(data['visible'])
    time = list(data['time'])
    cloud = list(data["cloud"])
    psm1 = list(data["psm1"])
    psm2 = list(data["psm2"])
    unstable = list(data['unstable'])

    # #exclude wrong plans (0 actions or unstable)
    force = [np.max(force[i]) for i in range(len(force)) if len(actions[i]) >= 3 and unstable[i] == 0] #MASSIMO VALORE DELLA FORZA
    distance = [distance[i] for i in range(len(distance)) if len(actions[i]) >= 3 and unstable[i] == 0]
    visible = [visible[i] for i in range(len(visible)) if len(actions[i]) >= 3 and unstable[i] == 0]
    time = [time[i] for i in range(len(time)) if len(actions[i]) >= 3 and unstable[i] == 0]
    cloud = [cloud[i] for i in range(len(cloud)) if len(actions[i]) >= 3 and unstable[i] == 0] 
    psm1 = [psm1[i] for i in range(len(psm1)) if len(actions[i]) >= 3 and unstable[i] == 0] 
    psm2 = [psm2[i] for i in range(len(psm2)) if len(actions[i]) >= 3 and unstable[i] == 0] 
    actions = [actions[i] for i in range(len(actions)) if len(actions[i]) >= 3 and unstable[i] == 0] 

    max_value_f = 100000000.
    old_force = copy.deepcopy(force)
    for i in range(len(force)):
        if visible[i] < min(80., max(visible)):
            force[i] = max_value_f
    
    if not any([f for f in force if f != max_value_f]):
        force = copy.deepcopy(old_force)
    
    index = []
    old_force = []
    num_forces = min(3, len([v for v in visible if v > 80. or v == max(visible)]))
    for i in range(num_forces):
        index.append(np.argmin(force))
        old_force.append(force[index[-1]])
        force[index[-1]] = max_value_f

    best = np.argmin([time[i] for i in index])

    for i in range(len(old_force)):
        force[index[i]] = old_force[i]
    plan = actions[index[best]]
    plan_list = []
    i=1
    for action in plan:
        plan_list.append([action.action, action.robot, action.object, action.color, i])
        i += 1

    np.savez(listener.plan_file + rospy.get_param("id_configuration"), time=time[index[best]], visible=visible[index[best]], force=force[index[best]], plan=plan_list, cloud=[cloud[index[best]]], psm1=[psm1[index[best]]], psm2=[psm2[index[best]]])

            




def best_plan_real(listener):
    file = listener.path_pkg + "/results/optimal_test_real" + rospy.get_param("id_configuration")[1:] + rospy.get_param("real_variant") + ".npz"
    data = np.load(file)
    force = list(data['force'])
    actions = list(data['actions'])
    distance = list(data['distance'])
    visible = list(data['visible'])
    time = list(data['time'])
    cloud = list(data["cloud"])
    psm1 = list(data["psm1"])
    psm2 = list(data["psm2"])
    unstable = list(data['unstable'])

    # #exclude wrong plans (0 actions or unstable)
    force = [np.max(force[i]) for i in range(len(force)) if len(actions[i]) >= 3 and unstable[i] == 0] 
    distance = [distance[i] for i in range(len(distance)) if len(actions[i]) >= 3 and unstable[i] == 0]
    visible = [visible[i] for i in range(len(visible)) if len(actions[i]) >= 3 and unstable[i] == 0]
    time = [time[i] for i in range(len(time)) if len(actions[i]) >= 3 and unstable[i] == 0]
    cloud = [cloud[i] for i in range(len(cloud)) if len(actions[i]) >= 3 and unstable[i] == 0] 
    psm1 = [psm1[i] for i in range(len(psm1)) if len(actions[i]) >= 3 and unstable[i] == 0] 
    psm2 = [psm2[i] for i in range(len(psm2)) if len(actions[i]) >= 3 and unstable[i] == 0] 
    actions = [actions[i] for i in range(len(actions)) if len(actions[i]) >= 3 and unstable[i] == 0] 
    unstable = [unstable[i] for i in range(len(unstable)) if unstable[i] == 0]

    max_value_f = 100000000.
    old_force = copy.deepcopy(force)
    for i in range(len(force)):
        if visible[i] < min(80., max(visible)):
            force[i] = max_value_f
    
    if not any([f for f in force if f != max_value_f]):
        force = copy.deepcopy(old_force)
    
    index = []
    old_force = []
    num_forces = min(3, len([v for v in visible if v > 80. or v == max(visible)]))
    for i in range(num_forces):
        index.append(np.argmin(force))
        old_force.append(force[index[-1]])
        force[index[-1]] = max_value_f

    best = np.argmin([time[i] for i in index])

    for i in range(len(old_force)):
        force[index[i]] = old_force[i]
    plan = actions[index[best]]
    plan_list = []
    i=1
    for action in plan:
        plan_list.append([action.action, action.robot, action.object, action.color, i])
        i += 1

    np.savez(listener.plan_file + rospy.get_param("id_configuration")[1:] + rospy.get_param("real_variant"), time=time[index[best]], visible=visible[index[best]], force=force[index[best]], plan=plan_list, cloud=[cloud[index[best]]], psm1=[psm1[index[best]]], psm2=[psm2[index[best]]])




    





if __name__ == '__main__':
    rospy.init_node("test_sofa")
    listener = Listener()
    rospy.sleep(1.)

    if "R" in rospy.get_param("id_configuration"):
        test_optimal_real(listener)
        best_plan_real(listener)
    else:
        test_optimal(listener)
        best_plan(listener)

    rospy.spin()