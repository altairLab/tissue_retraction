#!/usr/bin/env python2
import os
import rospkg
import rospy
import json
from std_msgs.msg import Int32, Bool


def on_shutdown():
    os.system("rosnode kill /control_node")
    os.system("ps -ef | grep 'control.py' | grep -v grep | awk '{print $2}' | xargs -r kill -9")
    



def main():

    rospy.init_node('TaskDVRK')
    rospy.on_shutdown(on_shutdown)

    rospack = rospkg.RosPack()
    path = rospack.get_path("robot_control") + '/config/'
    task = rospy.get_param("task_name")
    with open(path + task + ".json") as f:
        actions = json.load(f)["actions"]

    #DEFINE MOTION CONTROL
    if task == "retraction":
        from motion_retraction import motion_manager
    
    motion = motion_manager()

    test = rospy.get_param("test") #DON'T ASK FOR FLUENTS IF A PRE-COMPUTED PLAN IS AVAILABLE
    if test or rospy.get_param("study") == "_ismr":
        # ASK FOR FLUENTS
        motion.sensing_pub.publish(Int32(1))

    while not rospy.is_shutdown():

        rospy.loginfo('Waiting for the next action from the reasoner...')
        init_time = rospy.Time.now()
        while motion.state == []:
            if rospy.is_shutdown():
                break
            if rospy.Time.now() - init_time > rospy.Duration(10.):
                #ASK FOR FLUENTS
                motion.sensing_pub.publish(Int32(1))
                init_time = rospy.Time.now()

        for i in range(len(motion.state)):
            current_action = [a for a in actions if a["name"]==motion.state[i] and a["object"]==motion.location[i]][0]
            motion.agents.append(motion.manip_id[i])
            motion.policies.append(current_action["policy"])
            motion.policy_types.append(current_action["policy_type"])
            if current_action["policy_type"] == "dmp":
                motion.dmp_weights.append(current_action["weights"])
            motion.n_arms.append(current_action["n_arms"])

        # raw_input("CONFIRM")
        motion.execute()

        #FEEDBACK
        if not motion.failure:
            end_msg = Bool(True)
            
        else: #FAILURE -> COMPUTE NEW FLUENTS
            end_msg = Bool(False)

            #CHECKING FLUENTS
            rospy.loginfo('Waiting for the fluents from the camera...') 
            motion.computed_fluents = False
            motion.sensing_pub.publish(Int32(1))
            while not motion.computed_fluents:
                if rospy.is_shutdown():
                    break
                pass

        motion.reset()
        motion.end_pub.publish(end_msg)

    rospy.spin()


if __name__ == '__main__':
    main()