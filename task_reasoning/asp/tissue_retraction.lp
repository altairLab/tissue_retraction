% #include <incmode>.




#program base.

% problem entities
% #const n = 5. %size of the grid discretization
block((1..n, 1..n)). %set externally
robot(psm1;psm2).
object(block).

% context
#external fixed(B) : block(B).
#external reachable(R, block, B) : robot(R), block(B).
#external visible_tumor.
#external closed_gripper(R) : robot(R).
#external in_hand(R, block, B) : robot(R), block(B).
#external at(R, block, B) : robot(R), block(B).
#external max_height(R) : robot(R).
#external at_center(R) : robot(R).
#external above_tumor(B) : block(B). %which block is the ROI under
#external above_block(R, block, B) : robot(R), block(B).
#external discarded(B) : block(B).

%basic axioms
% adjacent(block((X,Y)), block((X+1,Y))) :- block((X,Y)), block(((X+1,Y))).
% adjacent(block((X,Y)), block((X,Y+1))) :- block((X,Y)), block(((X,Y+1))).
% adjacent(B1, B2) :- adjacent(B2, B1).
distance_blocks((X,Y), (X1,Y1), Z) :- block((X,Y)), block((X1,Y1)), Z = |X-X1| + |Y-Y1|.
distance_blocks(B1, B2, Z) :- distance_blocks(B2, B1, Z).
in_between((X1,Y1), (X2,Y2), (X3,Y3)) :- block((X1,Y1)), block((X2,Y2)), block((X3,Y3)), X3>=X2, Y3>=Y2, X1<=X3, Y1<=Y3, X1>=X2, Y1>=Y2.
in_between((X1,Y1), (X2,Y2), (X3,Y3)) :- block((X1,Y1)), block((X2,Y2)), block((X3,Y3)), X3>=X2, Y3<=Y2, X1<=X3, Y1>=Y3, X1>=X2, Y1<=Y2.
in_between(B1, B2, B3) :- in_between(B1, B3, B2).
min_dist(B1, B2, X) :- distance_blocks(B1, B2, X), fixed(B2), not fixed(B1), X = #min{Z : distance_blocks(B1, B2, Z), fixed(B2), not fixed(B1)}.

% initial conditions
at(R, block, B, 1) :- at(R, block, B).
in_hand(R, block, B, 1) :- in_hand(R, block, B).
visible_tumor(1) :- visible_tumor.
closed_gripper(R, 1) :- closed_gripper(R).
max_height(R, 1) :- max_height(R).
at_center(R, 1) :- at_center(R).
above_tumor(B, 1) :- above_tumor(B).



#program step(t).
%PRE-CONDITIONS
0{  reach(R, block, B, t) : reachable(R, block, B);
    grasp(R, block, B, t) : at(R, block, B, t);
    release(R, t) : closed_gripper(R, t);
    pull(R, block, B, t) : in_hand(R, block, B, t);
    move_away(R, block, B, t) : in_hand(R, block, B, t); %move horizontally towards center of tissue flap
    move_ROI(R, block, B, t) : in_hand(R, block, B, t) %move horizontally towards ROI
}1 :- not visible_tumor(t). %body only to avoid extra action after final check

%OPTIMIZATION
#minimize{X : reach(R, block, B, t), distance_blocks(B1, B, X), above_tumor(B1, t), flag_opt==0}. %move closer to ROI (faster exposure) 
#maximize{X : reach(R, block, B, t), min_dist(B, B2, X), flag_opt==0}.  %move farther from fixed points (maximize minimum distance between fixed and target block for reaching)
#maximize{1 : pull(R, block, B, t)}. %prefer pulling when possible
#minimize{1 : move_ROI(R, block, B, t)}. %prefer moving away away from camera, rather than to ROI

%CONSTRAINTS
%cannot grasp while already holding tissue
:- reach(_, block, _, t), closed_gripper(_, t).
%cannot operate on a discarded block
:- reach(_, block, B, t), discarded(B).
:- pull(_, block, B, t), discarded(B).
:- move_away(_, block, B, t), discarded(B).
:- move_ROI(_, block, B, t), discarded(B).
%cannot move higher than pre-defined limit
:- pull(R, block, _, t), max_height(R, t).
%cannot operate on a fixed block
:- pull(_, block, B, t), fixed(B).
:- move_ROI(_, block, B, t), fixed(B).
:- move_away(_, block, B, t), fixed(B).
%cannot move to ROI if already reached
:- move_ROI(_, block, B, t), above_tumor(B, t).
%cannot move to center if already done that
:- move_away(R, block, _, t), at_center(R, t).
%cannot expose a ROI under fixed block
:- visible_tumor(t), above_tumor(B, t), fixed(B).
%cannot move horizontally with fixed blocks on the way
% :- Z = #sum{1: move_ROI(R, block, B1, t), above_tumor(B2, t), in_between(B3, B1, B2), fixed(B3)}, Z > 0.


%EFFECTS
at(R, block, B, t) :- reach(R, block, B, t-1).
above_tumor(B, t) :- move_ROI(_, block, B, t-1).
above_tumor(B, t) :- above_tumor(B, t-1).
above_block(R, block, B, t) :- above_block(R, block, B, t-1), not move_ROI(R, block, _, t-1), not move_away(R, block, _, t-1).
at_center(R, t) :- move_away(R, block, _, t-1).
at_center(R, t) :- at_center(R, t-1), not move_ROI(R, block, _, t-1), not move_away(R, block, _, t-1).
closed_gripper(R, t) :- grasp(R, block, B, t-1).
closed_gripper(R, t) :- closed_gripper(R, t-1), not release(R, t-1).
in_hand(R, block, B, t) :- grasp(R, block, B, t-1).
in_hand(R, block, B, t) :- in_hand(R, block, B, t-1), not release(R, t-1).
max_height(R, t) :- max_height(R, t-1), not reach(_, block, _, t-1).
visible_tumor(t) :- pull(_, block, _, t-1).
visible_tumor(t) :- move_away(_, block, _, t-1).
% visible_tumor(t) :- pull(_, block, B, t-1), Z = #sum{1: move_ROI(R, block, B1, t), in_hand(R, block, B2, t-1), in_between(B3, B1, B2), fixed(B3)}, Z < 1, not discarded(B).
visible_tumor(t) :- move_ROI(_, block, _, t-1).












#program check(t).

#external query(t). % for python interface
:- not visible_tumor(t), query(t).











% output
#show reach/4.
#show move_ROI/4.
#show move_away/4.
#show grasp/4.
#show pull/4.
#show release/2.

