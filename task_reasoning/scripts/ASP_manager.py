#!/usr/bin/env python

import rospy
import rospkg
from clingo import Function as Fun, Control
from dvrk_task_msgs.msg import ContextModel, ActionRequestFiner, ActionArray
from std_msgs.msg import Bool
import copy
import numpy as np
import os
from gringoParser import string2fun as parse



class Listener(object):
    def __init__(self):
        self.feedback = None
        self.context = None
        self.started_control = False
        self.received_context = False
        rospy.Subscriber(rospy.get_param("context_topic"), ContextModel, self.context_cb)
        rospy.Subscriber(rospy.get_param("action_feedback_topic"), Bool, self.feedback_cb)
        rospy.Subscriber("started_control", Bool, self.control_cb)
        self.action_pub = rospy.Publisher(rospy.get_param("action_topic"), ActionArray, queue_size=1)


    def feedback_cb(self, data):
        self.feedback = data.data

    def context_cb(self, data):
        self.context = copy.deepcopy(data)
        self.received_context = True

    def control_cb(self, data):
        self.started_control = data.data


class Solver(object):    
    def __init__(self, filename, init_plan=[]):
        self.filename = filename

        if not rospy.get_param("test"): #NO OPTIMIZATION FAR FROM APs AND CLOSE TO ROI
            self.control = Control(["-c n=" + str(rospy.get_param("grid_size")), "-c flag_opt=0", "--opt-mode=opt"])
        else:
            self.control = Control(["-c n=" + str(rospy.get_param("grid_size")), "-c flag_opt=1", "--opt-mode=opt"])
        path = rospkg.RosPack().get_path('task_reasoning')
        self.control.load(path + '/asp/' + self.filename)
        self.control.ground([("base", [])])        
        self.atoms = []
        self.hidden_atoms = []
        self.ordered_atoms = list(init_plan)
        self.action_time = 0  
        self.action_index = 0
        self.step = len(self.ordered_atoms) + 1 #for iterative solver

        self.unsat_pub = rospy.Publisher("/unsat_test", Bool, queue_size=1)

    def restart(self):
        if not rospy.get_param("test"): #NO OPTIMIZATION FAR FROM APs AND CLOSE TO ROI
            self.control = Control(["-c n=" + str(rospy.get_param("grid_size")), "-c flag_opt=0", "--opt-mode=opt"])
        else:
            self.control = Control(["-c n=" + str(rospy.get_param("grid_size")), "-c flag_opt=1", "--opt-mode=opt"])
        path = rospkg.RosPack().get_path('task_reasoning')
        self.control.load(path + '/asp/' + self.filename)
        self.control.ground([("base", [])])        
        self.atoms = []
        self.hidden_atoms = []
        self.step = 1 #for iterative solver
        

    def on_model(self, model):
        self.atoms[:] = model.symbols(shown=True)
        self.hidden_atoms[:] = model.symbols(atoms=True)

    def solve(self, context):
        self.ordered_atoms = copy.deepcopy(self.ordered_atoms[0:self.action_index])
        #DELETE EXTERNALS FROM PREVIOUS CALL
        for atom in self.hidden_atoms:
            self.control.assign_external(atom, False)
        #GROUND NEW EXTERNALS
        for atom in context.atoms:
            self.control.assign_external(parse(atom), True)

        init_time = rospy.Time.now()
        while rospy.Time.now() - init_time < rospy.Duration(secs=50.):
            parts = []

            self.control.cleanup()
            parts.append(("check", [self.step]))
            self.control.release_external(Fun("query", [self.step-1]))

            parts.append(("step", [self.step]))

            self.control.ground(parts)
            self.control.assign_external(Fun("query", [self.step]), True)
            result = self.control.solve(on_model = self.on_model)
            self.step += 1
            if result.satisfiable:
                break

        if result.satisfiable:
            if self.atoms != []:
                if not rospy.get_param("test"):
                    rospy.loginfo("FOUND AN ANSWER SET!")
                for atom in self.atoms:
                    tmp_list = ['none', 'none', 'none', 'none', 0]
                    tmp_list[0] = atom.name # name...
                    for i in range(len(atom.arguments)-1): # ...arguments...
                        tmp_list[i+1] = str(atom.arguments[i])
                    tmp_list[-1] = int(str(atom.arguments[-1])) + self.action_time # ...time
                    self.ordered_atoms.append(tmp_list)

                self.ordered_atoms.sort(key = lambda action: int(action[-1]))
                rospy.loginfo('SEQUENCE OF POSSIBLE ACTIONS IS: ')
                rospy.loginfo(self.ordered_atoms)

        else:
            rospy.logwarn('UNSATISFIABLE PLANNING!')
            if not rospy.get_param("test") and rospy.get_param("grid_size") == 5:
                rospy.set_param("grid_size", 7)
            else:
                self.unsat_pub.publish(Bool(True))

    def get_action_msg(self):
        actions = ActionArray()
        while len(self.ordered_atoms) > self.action_index and int(self.ordered_atoms[self.action_index][-1]) == self.action_time + 1:
            action = ActionRequestFiner()
            action.robot = str(self.ordered_atoms[self.action_index][1])
            action.action = str(self.ordered_atoms[self.action_index][0])
            action.object = str(self.ordered_atoms[self.action_index][2])
            action.color = str(self.ordered_atoms[self.action_index][3])

            actions.action_list.append(action)
            self.action_index += 1

        if actions != ActionArray():
            self.action_time += 1
        return actions





def main():
    rospy.init_node('ASP_manager')
    filename = rospy.get_param("asp_name")

    listener = Listener()

    init_plan = []
    if not rospy.get_param("test") and rospy.get_param("study") != "_ismr": #PRE-COMPUTED PLAN AVAILABLE, READ IT FROM FILE
        path = rospkg.RosPack().get_path("robot_control")
        if "R" in rospy.get_param("id_configuration"):
            data = np.load(path + "/results/plan_comparison/plan"+rospy.get_param("id_configuration")[-1]+rospy.get_param("real_variant")+".npz")
        else:
            data = np.load(path + "/results/plan_comparison/plan"+rospy.get_param("id_configuration")+".npz")
    
        init_plan = data["plan"]
        rospy.loginfo(init_plan)
        
    solver = Solver(filename, init_plan)

    while not listener.started_control:
        pass

    while not rospy.is_shutdown():
        #GETTING FLUENTS
        if listener.received_context:
            rospy.loginfo('FLUENTS ARE:')
            rospy.loginfo(listener.context)
            solver.restart()
            solver.solve(listener.context)
            listener.received_context = False

        action = solver.get_action_msg()
        
        if action != ActionArray():
            listener.action_pub.publish(action)
            #ACTION FEEDBACK
            rospy.loginfo("WAITING FOR ACTION FEEDBACK...")
            while listener.feedback is None:
                if rospy.is_shutdown():
                    break
                pass
            listener.feedback = None      

    rospy.spin()








if __name__ == '__main__':
    main()
