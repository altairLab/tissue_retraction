""" 
Script for ROI segmentation from real endoscopic images
"""
#!/usr/bin/env python
import os, sys
import numpy as np

# ROS IMPORT
import rospy, rospkg
from std_msgs.msg import Header, Float32, Float32MultiArray
from geometry_msgs.msg import PointStamped, TransformStamped, PoseStamped
from dvrk_task_msgs.msg import ActionArray
import tf

# Image processing
import cv2
from cv_bridge import CvBridge
from threading import RLock
from sensor_msgs.msg import CompressedImage, CameraInfo
import image_geometry

#################################################
class ImageProcessing(object):
	def __init__(self):

		self.debug = True 	

		# If endoscope calibration is not reliable, target coordinates MUST be defined directly on the image in pixel coordinates
		self.is_endoscope_reliable = False
		target2image = (530,405)

		# Segmentation
		# red
		self.hsv_min = np.asarray([0, 0, 0])
		self.hsv_max = np.asarray([20, 255, 255])

		self.cv_bridge  = CvBridge()
		self.lock_frame = RLock()
		
		# Brightness and contrast
		self.processing = True
		self.alpha = 3.0 # contrast control [1.0-3.0]
		self.beta  = 10.0 # brightness control [0-100]

		# Camera model
		print 'Waiting for the camera info...'
		self.camera_model = image_geometry.PinholeCameraModel()
		camera_info_mg    = rospy.wait_for_message(rospy.get_param("image_info_topic"), CameraInfo)
		self.camera_model.fromCameraInfo(camera_info_mg)
		
		if self.is_endoscope_reliable:

			# Expected ROI position in world coordinates
			print 'Waiting for target position from simulation...'
			target_msg 			 = rospy.wait_for_message(rospy.get_param("roi_position_topic"), Float32MultiArray)		
			target_point = PointStamped()
			target_point.header.frame_id = rospy.get_param("fixed_frame")
			target_point.header.stamp 	 = rospy.Time(0)
			target_point.point.x         = target_msg.data[0]
			target_point.point.y         = target_msg.data[1]
			target_point.point.z         = target_msg.data[2]
			print 'Target in world coordinates: ', target_msg.data

			tf_list = tf.TransformListener()
			tf_list.waitForTransform("/ECM", rospy.get_param("fixed_frame"), time=rospy.Time(0.), timeout = rospy.Duration(5.))
			target2ecm = tf_list.transformPoint("/ECM", target_point)

			# Handeye calibration 
			handeye_tq = rospy.get_param("handeye")
			handeye_tf = create_transform(handeye_tq, self.camera_model.tfFrame(), "/ECM")
			# print self.camera_model.tfFrame()
			#tf_list.waitForTransform(self.camera_model.tfFrame(), "/ECM", time=rospy.Time(0.), timeout = rospy.Duration(5.))
			
			tf_list.setTransform(handeye_tf)
			t2c = tf_list.transformPoint(self.camera_model.tfFrame(), target2ecm)
			target2camera = [t2c.point.x, t2c.point.y, t2c.point.z]
			target2image = self.camera_model.project3dToPixel(target2camera)
			target2image = (int(target2image[0]), int(target2image[1]))
		else:
			# If we want to manually place the target on image plane
			print 'Target position manually set!'
		
		roi_radius   = 25
		print 'Target position in image coordinates: ', target2image

		circle_mask = np.zeros((self.camera_model.height,self.camera_model.width), dtype="uint8")
		cv2.circle(circle_mask, target2image, roi_radius, 255, -1)
		self.circle_mask = circle_mask
		self.roi_total_points = np.count_nonzero(self.circle_mask)

		print 'Waiting for the image...'

		# Publisher of visiblity metric
		self.visibility_pub = rospy.Publisher(rospy.get_param("roi_visibility_topic"), Float32, queue_size=1)
		self.image_pub = rospy.Publisher("/image_target", CompressedImage, queue_size=1)

		# Subscribers to ecm camera
		image_sub   = rospy.Subscriber(rospy.get_param("image_topic"), CompressedImage, self.image_callback)

	def image_callback(self, msg):

		try:
			self.lock_frame.acquire()
			img = self.cv_bridge.compressed_imgmsg_to_cv2(msg, "passthrough") #means, encoding is the same as imgmsg: BGR8
			
			if self.processing:
				new_img = cv2.convertScaleAbs(img, alpha=self.alpha, beta=self.beta)
				img = new_img
						
			# Segment by color
			mask = roi_segmentation(img, self.hsv_min, self.hsv_max)
			
			# Combine circle and segmentation
			roi_mask = cv2.bitwise_and(mask, mask, mask=self.circle_mask)
			visibility = compute_visibility(roi_mask, self.roi_total_points)
			print 'Visibility: ', visibility
			self.visibility_pub.publish(Float32(visibility))

			if self.debug:

				cv2.imshow("Mask", roi_mask)
				cv2.waitKey(3)

				circle_res = cv2.cvtColor(self.circle_mask, cv2.COLOR_GRAY2BGR)
				img_with_roi = cv2.addWeighted(img, 1, circle_res, 0.9, 0)
				cv2.imshow("Image with roi", img_with_roi)

			self.lock_frame.release()

		except (cv2.error):
			self.lock_frame.release()
			rospy.logerr("Failed to decode image!")


def roi_segmentation(img, hsv_min, hsv_max, roi=None):

	img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
	mask 	= cv2.inRange(img_hsv, hsv_min, hsv_max)

	if roi is not None:
		# put to zero all points outside roi
		pass

	return mask

def compute_visibility(mask, total_points):
	nonzero_pixels = np.count_nonzero(mask)
	return 100*nonzero_pixels/total_points
	
def create_transform(tq, from_frame, to_frame):
	# Create transform stamped from a list tq containing translation and quaternion
	tf = TransformStamped()
	tf.header.frame_id = to_frame
	tf.child_frame_id  = from_frame
	tf.header.stamp    = rospy.Time(0)
	tf.transform.translation.x  = tq[0]
	tf.transform.translation.y  = tq[1]
	tf.transform.translation.z  = tq[2]
	tf.transform.rotation.x     = tq[3]
	tf.transform.rotation.y     = tq[4]
	tf.transform.rotation.z     = tq[5]
	tf.transform.rotation.w     = tq[6]
	return tf

def main():

	rospy.init_node('image_processing_node')
	m = ImageProcessing()

	r = rospy.Rate(1)
	rospy.sleep(1.)

	while not rospy.is_shutdown():
		r.sleep()

if __name__ == '__main__':
	main()


