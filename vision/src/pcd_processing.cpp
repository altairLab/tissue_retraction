
#include <ros/ros.h>
#include <ros/package.h>
#include <tf/transform_listener.h>
#include <iostream>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseStamped.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/uniform_sampling.h>
#include <pcl/conversions.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/random_sample.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>

void PointRGBtoHSV(pcl::PointXYZRGB &in, pcl::PointXYZHSV &out)
{
    const unsigned char max = std::max (in.r, std::max (in.g, in.b));
    const unsigned char min = std::min (in.r, std::min (in.g, in.b));
    out.x = in.x; out.y = in.y; out.z = in.z;
    out.v = static_cast <float> (max) / 255.f;

    if (max == 0) // division by zero
    {
        out.s = 0.f;
        out.h = 0.f; // h = -1.f;
        return;
    }

    const float diff = static_cast <float> (max - min);
    out.s = diff / static_cast <float> (max);

    if (min == max) // diff == 0 -> division by zero
    {
        out.h = 0;
        return;
    }

    if      (max == in.r) out.h = 60.f * (      static_cast <float> (in.g - in.b) / diff);
    else if (max == in.g) out.h = 60.f * (2.f + static_cast <float> (in.b - in.r) / diff);
    else                  out.h = 60.f * (4.f + static_cast <float> (in.r - in.g) / diff); // max == b

    if (out.h < 0.f) out.h += 360.f;
}

void PcdRGBtoHSV(pcl::PointCloud<pcl::PointXYZRGB>& in, pcl::PointCloud<pcl::PointXYZHSV>& out)
{
    out.width   = in.width;
    out.height  = in.height;
    for (size_t i = 0; i < in.points.size (); i++)
    {
        pcl::PointXYZHSV p;
        PointRGBtoHSV (in.points[i], p);
        out.points.push_back (p);
    }
}

class pcd_proc
{
    private:
        // Ros handler
        ros::NodeHandle nh_;
        ros::NodeHandle private_nh_;
        std::string fixed_frame, optical_frame, pcd_input_topic, pcd_output_topic;
        tf::StampedTransform optical2map;
        tf::TransformListener listener;

        std::vector<double> hsv_range, bb_coords;
        double us_radius, mls_radius;
        int seed, target_number;

        pcl::PointCloud<pcl::PointXYZRGB>::Ptr xyz_cld_ptr, segmented_cloud;

        sensor_msgs::PointCloud2 output_cloud_msg;

        ros::Publisher cloud_pub;
        ros::Subscriber cloud_sub;

    protected:
        virtual ros::NodeHandle &getNodeHandle() { return nh_; }
        virtual ros::NodeHandle &getPrivateNodeHandle() { return private_nh_; }

    public:
        pcd_proc(ros::NodeHandle &nh);

        ~pcd_proc() {}

        void init();
        void update();

        pcl::PointCloud<pcl::PointXYZRGB>::Ptr colorSegmentation(pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud);

        void cloudCallback(const sensor_msgs::PointCloud2ConstPtr &input)
        {
            pcl::PCLPointCloud2 pcl_pc2;             //struttura pc2 di pcl
            pcl_conversions::toPCL(*input, pcl_pc2); //conversione a pcl della pc2

            pcl_pc2.fields[3].datatype = sensor_msgs::PointField::FLOAT32;

            pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
            pcl::fromPCLPointCloud2(pcl_pc2, *cloud);
            try
            {
                listener.waitForTransform(optical_frame, fixed_frame, ros::Time(0), ros::Duration(5.0));
                listener.lookupTransform(fixed_frame, optical_frame, ros::Time(0), optical2map);

                pcl_ros::transformPointCloud(*cloud, *xyz_cld_ptr, optical2map);
                xyz_cld_ptr->header.frame_id = fixed_frame;
            }
            catch (tf::TransformException ex)
            {
                ROS_ERROR("%s", ex.what());
            }

            update();
        }
};

pcd_proc::pcd_proc(ros::NodeHandle &nh) : nh_(nh), private_nh_("~"),
                                            xyz_cld_ptr(new pcl::PointCloud<pcl::PointXYZRGB>),
                                            segmented_cloud(new pcl::PointCloud<pcl::PointXYZRGB>)
{
}

pcl::PointCloud<pcl::PointXYZRGB>::Ptr pcd_proc::colorSegmentation(pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud)
{
    ROS_WARN("Original pcl: %d", cloud->size());

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr segmented_cloud_tmp(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr xyz_f_(new pcl::PointCloud<pcl::PointXYZRGB>);
    std::vector<int> indices;
    pcl::removeNaNFromPointCloud(*cloud, *xyz_f_, indices);

    ROS_WARN("After removing nans: %d", xyz_f_->size());

    // APPLY BOUNDING BOX
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr decimated_cloud_tmp(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices());
    pcl::ExtractIndices<pcl::PointXYZRGB> extract;
    for (size_t i = 0; i < xyz_f_->size(); i++)
    {
        float x = xyz_f_->points[i].x;
        float y = xyz_f_->points[i].y;
        float z = xyz_f_->points[i].z;

        if ((x <= bb_coords[1] && x >= bb_coords[0]) && (y <= bb_coords[3] && y >= bb_coords[2]) && (z <= bb_coords[5] && z >= bb_coords[4])) {
            inliers->indices.push_back(i);
        }
    }
    extract.setInputCloud(xyz_f_);
    extract.setIndices(inliers);
    extract.setNegative(false);
    extract.filter(*decimated_cloud_tmp);

    ROS_WARN("After bounding box: %d", decimated_cloud_tmp->size());

    // COLOR SEGMENTATION
    pcl::PointCloud<pcl::PointXYZHSV>::Ptr HSV(new pcl::PointCloud<pcl::PointXYZHSV>);
    PcdRGBtoHSV(*decimated_cloud_tmp, *HSV); // RGB -> HSV
    segmented_cloud_tmp->points.clear();

    for (size_t i = 0; i < HSV->points.size(); i++) {
        bool first = HSV->points[i].h > hsv_range[0] && HSV->points[i].h < hsv_range[1];
        bool second = HSV->points[i].h > hsv_range[2] && HSV->points[i].h < hsv_range[3];

        if (( first || second ) && (HSV->points[i].s >= hsv_range[4] && HSV->points[i].s <= hsv_range[5]))
        {
            pcl::PointXYZRGB out_points;
            out_points.x = HSV->points[i].x;
            out_points.y = HSV->points[i].y;
            out_points.z = HSV->points[i].z;
            out_points.r = decimated_cloud_tmp->points[i].r;
            out_points.g = decimated_cloud_tmp->points[i].g;
            out_points.b = decimated_cloud_tmp->points[i].b;
            segmented_cloud_tmp->push_back(out_points);
        }
    }

    ROS_WARN("After color segmentation: %d", segmented_cloud_tmp->size());

    // DECIMATION (DONE ONLY AFTER HAVING THE POINT OF THE TISSUE)
    auto us = pcl::UniformSampling<pcl::PointXYZRGB>();
    us.setInputCloud(segmented_cloud_tmp);
    us.setRadiusSearch(us_radius);
    us.filter(*decimated_cloud_tmp);
    ROS_WARN("After uniform sample decimation: %d", decimated_cloud_tmp->size());
    
    // FINAL DECIMATION TO REACH A TARGET NUMBER OF POINTS
    auto rs = pcl::RandomSample<pcl::PointXYZRGB>(false);
    rs.setInputCloud(decimated_cloud_tmp);
    rs.setSample(target_number);
    rs.setSeed(seed);
    rs.filter(*decimated_cloud_tmp);

    ROS_WARN("After random decimation (target 2k): %d", decimated_cloud_tmp->size());

    // MLS FOR SMOOTHING
    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB> mls_points;

    pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGB> mls; 
    mls.setComputeNormals (false);
    mls.setInputCloud (decimated_cloud_tmp);
    mls.setPolynomialFit(false);
    //mls.setPolynomialOrder (5);
    mls.setSearchMethod (tree);
    mls.setSearchRadius (mls_radius);
    mls.process (mls_points);

    // Convert back to Ptr    
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr mls_cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    *mls_cloud = mls_points;

    //ROS_WARN("After MLS: %d", mls_cloud->size());
    
    mls_cloud->header.frame_id = fixed_frame;
    mls_cloud->header.stamp = xyz_cld_ptr->header.stamp;
    return mls_cloud;
}

void pcd_proc::init()
{

    private_nh_.param("fixed_frame", fixed_frame, std::string("/world"));
    private_nh_.param("optical_frame", optical_frame, std::string("/camera_color_optical_frame"));
    private_nh_.param("pcd_input_topic", pcd_input_topic, std::string("/camera/depth_registered/points"));
    private_nh_.param("pcd_output_topic", pcd_output_topic, std::string("/postproc_cloud"));

    private_nh_.getParam("us_radius", us_radius);
    private_nh_.getParam("seed", seed);

    private_nh_.getParam("target_number", target_number);
    private_nh_.getParam("hsv_range", hsv_range);
    private_nh_.getParam("bb_coords", bb_coords);

    private_nh_.getParam("mls_radius", mls_radius);

    cloud_sub = nh_.subscribe(pcd_input_topic, 1, &pcd_proc::cloudCallback, this);

    cloud_pub = nh_.advertise<sensor_msgs::PointCloud2>(pcd_output_topic, 1, this);
}

void pcd_proc::update()
{
    //pcl::PointCloud<pcl::PointXYZRGB>::Ptr map_cld_ptr(new pcl::PointCloud<pcl::PointXYZRGB>);

    if (xyz_cld_ptr->size() > 0)
    {
        segmented_cloud = colorSegmentation(xyz_cld_ptr);

        pcl::toROSMsg(*segmented_cloud, output_cloud_msg);
        cloud_pub.publish(output_cloud_msg);
    }
}

// -----------------------------------------------------------------
int pcd_processing(int argc, char **argv)
{
    ros::init(argc, argv, "pcd_processing_node");
    ros::NodeHandle nh;
    pcd_proc pcd_proc(nh);
    pcd_proc.init();

    while (ros::ok())
    {
        // pcd_proc.update();
        ros::spinOnce();
        ros::Duration(0.1).sleep();
    }
    return 0;
}

int main(int argc, char **argv)
{
    return pcd_processing(argc, argv);
}