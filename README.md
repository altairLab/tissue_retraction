# DEFRAS: DEliberative Framework for Robot Assisted Surgery

This repository (v2.0) contains the modules required by DEFRAS and implements tissue retraction task both in simulation and real da Vinci Research Kit (dVRK) as described in ["Deliberation in autonomous robotic surgery: a framework for handling anatomical uncertainty"](https://ieeexplore.ieee.org/document/9811820).

Overview:
* [Setup](#Setup)
* [Description](#Description)
* [Run](#Run)
* [References](#References)

## Setup
This repo has been tested using ROS melodic (ubuntu 18.04) and python2.
##### ROS packages 
This repo depends on the following ROS packages:
- **cisst libraries**
    - [cisst](https://github.com/jhu-cisst/cisst/tree/1.0.11) v1.0.11. In case you have problems building cisst because of missing header files, a temporary workaround is to go to the build/cisst directory and run `make -j -k` a few times. Once the headers have been generated, you can resuem the normal build process using `catkin build`.
    - [cisst-ros](https://github.com/jhu-cisst/cisst-ros/tree/1.5.0) v1.5.0
    - [cisstNetlib](https://github.com/jhu-cisst/cisstNetlib/tree/d9ba95d51c06185ee5df3a89fb30cc3b5fe04214)
- **saw libraries**
    - [sawControllers](https://github.com/jhu-saw/sawControllers/tree/1.7.0) v1.7.0
    - [sawRobotIO1394](https://github.com/jhu-saw/sawRobotIO1394/tree/1.7.0) v1.7.0. This in turns depends on [Amp1394](https://github.com/jhu-cisst/mechatronics-software/tree/1.5.0) v1.5.0, to be cloned inside sawRobotIO1394/components/code/Amp1394
    - [sawTextToSpeech](https://github.com/jhu-saw/sawTextToSpeech/tree/1.2.0) v1.2.0
    - [sawIntuitiveResearchKit](https://github.com/jhu-dvrk/sawIntuitiveResearchKit/tree/1.7.0) v1.7.0
- [dvrk ros](https://github.com/jhu-dvrk/dvrk-ros/tree/1.7.0) v1.7.0

Please note that building other versions of the packages will not work. 
##### Clingo 
Download and install Clingo (v5.3.0) following instructions at https://github.com/potassco/clingo (v5.3.0 is required for support with python2). Remember to add the PATH-TO-CLINGO/bin/python to your $PYTHONPATH to enable python to find the required modules.
##### SOFA 
Download and install SOFA (v20.06), following the instructions at https://www.sofa-framework.org/community/doc/getting-started/build/linux/. Simulation is implemented relying on SofaPython plugin, which needs to be manually activated in CMake (thus, python3 is not supported). Once built, SOFA can be launched with /PATH-TO-BUILD/bin/runSofa (you can either add runSofa to your ~/.bashrc or create a symlink). Required python modules for simulations can be found in simulation/requirements.txt.

## Description
- task_reasoning contains *Task planning* module (ASP)
- robot_control contains *Acting* module and tools for dvrk control
- situation_awareness contains *Sensing*, *Monitoring* and *Learning* modules
- vision contains tools for RGBD point cloud processing and endoscopic image processing
- simulation contains tools to obtain the *Simulated environment* with SOFA
- dvrk_task_msgs contains custom msgs (action msgs expect fields defining the action id, the agent robot, the operated object and its property, as of standard granularity definitions for actions). **Note**: when running nodes on multiple machines, dvrk_task_msgs must be built on all of them.

## Run
All tests require an active `roscore`. 
The main configuration file for DEFRAS is `parameters.launch` in robot_control package. The most important parameters of the framework are set there, so please carefully check that file (and launch it) before each run.

### Find optimal plan for simulated scenarios
Update the file *robot_control/launch/parameters.launch* by setting:
- test: True
- id_configuration to the ID of the configuration of APs to test (a number from 0 to 7).

Then, you have to
1. Setup framework parameters by launching `roslaunch robot_control parameters.launch`
2. Start the dvkr with `roslaunch robot_control dvrk_console.launch`
3. Launch the script *robot_control/scripts/optimal_generate.py*

### Validation in simulation
This step requires two simulations running in parallel: one representing the simulated environment (*pre-operative simulation*) and the other representing the real environment (*actual simulation*).

<div align="center">
  <img width="700" height="100" src="figures/config-sim.png" alt="Configurations used in simulated experiments">
</div>

Update the file *robot_control/launch/parameters.launch* by setting:
- study to the appropriate value between ["", "_ismr"] depending if you want to use DEFRAS or FRAS, respectively
- test: False
- id_configuration to the ID of the pre-operative scenario to consider (a number from 0 to 7). This will be used to define APs in the *pre-operative simulation*
- real_variant to the desired actual AP configuration ["0", "1"], with fewer (< in paper) or more (> in paper) APs than the pre-operative configuration. This will be used to define APs in the *actual simulation*

Then, to start the framework on the **simulated** system, you have to
1. Setup framework parameters by launching `roslaunch robot_control parameters.launch`
2. Start the dvkr with `roslaunch robot_control dvrk_console.launch` (press "Power on" and then "Home" in the GUI)
4. Start the simulated environment (*pre-operative simulation*): `runSofa simulation.py`
3. Start the simulation of the real environment (*actual simulation*): `runSofa actual_simulation.py` 
5. Start banet `python banet_inference.py` from situation_awareness/scripts
6. Start the reasoning node with `roslaunch robot_control framework.launch` 
7. Optional: you can visualize robot configuration in rviz with the scene robot_control/rviz_config/scene_real.rviz

### Find optimal plan for real scenarios, in offline simulation
Update the file *robot_control/launch/parameters.launch* by setting:
- test: True
- id_configuration to the ID of the real world scenario to test ["R0", "R1", "R2"] (which correspond to scenarios A, B, C in the paper).
- real_variant to the desired configuration of APs for which we want to generate the plan ["0", "1", "ground"] (which correspond to <, >, = in the paper).

Then, you have to
1. Setup framework parameters by launching `roslaunch robot_control parameters.launch`
2. Start the dvkr with `roslaunch robot_control dvrk_console.launch`
3. Launch the script *robot_control/scripts/optimal_generate.py* 

### Validation on dVRK
<div align="center">
  <img width="300" height="130" src="figures/config-real.png" alt="Configurations used in real world experiments">
</div>

Update the file *robot_control/launch/parameters.launch* by setting:
- study to the appropriate value between ["", "_ismr"] depending if you want to use DEFRAS or FRAS, respectively
- test: False
- id_configuration to the ID of the real world scenario to test ["R0", "R1", "R2"] (which correspond to scenarios A, B, C in the paper). 
- real_variant to the desired pre-operative scenario to consider ["0", "1", "ground"] (which correspond to <, >, = in the paper). This will be used to define APs in the *pre-operative simulation*

Then, to start the framework on the **real** system, you have to
1. Setup framework parameters by launching `roslaunch robot_control parameters.launch`
2. Start the real dVRK
3. Start the simulated environment: `runSofa simulation.py`
4. Start vision node by `roslaunch vision pcd_processing.launch` for RGBD point cloud processing
5. Start image_processing script for visibility from endoscope `python image_processing.py` from vision/scripts
6. Start banet `python banet_inference.py` from situation_awareness/scripts
7. Start the reasoning node with `roslaunch robot_control framework.launch` 
8. Optional: you can visualize robot configuration in rviz with the scene robot_control/rviz_config/scene_real.rviz


## References
Tagliabue E., Meli D., Dall'Alba D and Fiorini P., ["Deliberation in autonomous robotic surgery: a framework for handling anatomical uncertainty"](https://ieeexplore.ieee.org/document/9811820), IEEE Int. Conf. on Robotics and Automation (2022).

Altair Robotics Lab - University of Verona

*Contacts*: 
- eleonora[dot]tagliabue[at]univr[dot]it
- daniele[dot]meli[at]univr[dot]it 

## License
Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
